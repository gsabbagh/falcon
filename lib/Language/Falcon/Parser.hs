{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

{-| Module  : falcon
Description : Parser for language
Copyright   : Alice Demonfaucon 2023
License     : LGPL-3
Maintainer  : alice.demonfaucon@etu.utc.fr
Stability   : experimental
Portability : portable

Parser for the Falcon programming language.

-}

module Language.Falcon.Parser
(
    -- * Datatypes
    
    FalconToken(..),
    ParsedFalconFile(..),
    Identifier(..),
    -- ** Types for categories
    IdCat (..),
    IdObject (..),
    IdArrow (..),
    Composition (..),
    CategoryStatement (..),
    CategoryDeclaration (..),
    -- ** Types for functors
    IdFunctor (..),
    FunctorStatement (..),
    FunctorToSketchStatement(..),
    FunctorDeclaration (..),
    -- ** Types for cones
    IdCone (..),
    ConeStatement (..),
    ConeDeclaration (..),
    -- ** Types for cocones
    IdCocone (..),
    CoconeStatement (..),
    CoconeDeclaration (..),
    -- ** Types for exponential objects
    IdExp (..),
    ExponentialDeclaration (..),
    -- ** Types for sketches
    IdSketch (..),
    SketchStatement(..),
    RenamingStatement(..),
    SketchDeclaration(..),
    -- ** Types for sketch morphisms
    IdSmorphism (..),
    SmorphismDeclaration(..),
    -- ** Types for imports
    ImportStatement (..),
    -- ** Types for models
    IdModel(..),
    ForeignCode(..),
    SupportedIntermediateLanguage(..),
    ModelDeclaration(..),
    
    -- * Parsers
    parseCategoryDeclaration,
    parseFunctorDeclaration,
    parseConeDeclaration,
    parseCoconeDeclaration,
    parseExponentialDeclaration,
    parseSketchDeclaration,
    parseSketchMorphismDeclaration,
    parseImportStatement,
    parseFalcon,
    
    -- * Tests
    testCategoryDeclaration1,
    testCategoryDeclaration2,
    testCategoryDeclaration3,
    testCategoryDeclaration4,
    testFunctor1,
    testFunctor2,
    testCone1,
    testCocone1,
    testExponentialObject1,
    testSketchDeclaration1,
    testSketchDeclaration2,
    testSketchDeclaration3,
    testSketchMorphism1,
    testImport1,
    testImport2,
    testImport3,
    testImport4,
    testImport5,
    testImport6,
    testFalcon1,
)
where

    import            Text.Parsec
    import            Text.Parsec.Language
    import qualified  Text.Parsec.Token      as P
    
    import            Control.Applicative    (liftA2, liftA3)
    import            Control.Monad
    
    import            Data.Functor.Identity
    import            Data.Simplifiable
    
    import            GHC.Generics
    
    import            Math.IO.PrettyPrint

    -- * FALCON TYPES

    -- | A token for the falcon programming language.
    data FalconToken =  Import ImportStatement
                        | Category CategoryDeclaration
                        | Sketch SketchDeclaration
                        | Functor FunctorDeclaration
                        | Smorphism SmorphismDeclaration
                        | Cone ConeDeclaration
                        | Cocone CoconeDeclaration
                        | Exponential ExponentialDeclaration
                        | Model ModelDeclaration
                        deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)


    -- * Types of parsed statements
    -- All identifiers are String, we specify types for each identifier for readability.
    
    -- | Any identifier is a string, we will define different types of identifiers for readability.
    type Identifier = String
    
    -- * Types for categories

    -- | An object identifier is a String.
    type IdObject = String
    -- | An arrow identifier is a String.
    type IdArrow = String
    -- | A composition is a list of arrow.
    type Composition = [IdArrow]
    -- | A category identifier is a String.
    type IdCat = String
    -- | A category statement is the declaration of an object or an arrow or a composition law entry.
    data CategoryStatement = Object IdObject | Arrow IdArrow IdObject IdObject | Law Composition Composition deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    -- | A category declaration is a category created from scratch thanks to a list of 'CategoryStatement's.
    data CategoryDeclaration = CategoryDeclaration IdCat [CategoryStatement] deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    -- * Types for functors
    
    -- | A functor identifier is a String.
    type IdFunctor = String
    -- | A functor statement is a mapping of object or a mapping of arrow to a morphism.
    data FunctorStatement = MapObject IdObject IdObject | MapArrow IdArrow Composition deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    -- | A functor to Sketch statement is a mapping of object to a category or a mapping of arrow to a functor.
    data FunctorToSketchStatement = MapObjectToSketch IdObject IdSketch | MapArrowToSmorphism IdArrow IdSmorphism deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    -- | A functor declaration is a functor created from scratch, it can be a functor from a user created category to another user created categoru, to Cat or to Sketch.
    data FunctorDeclaration = FunctorDeclaration IdFunctor IdCat IdCat [FunctorStatement] | FunctorToSketchDeclaration IdFunctor IdCat [FunctorToSketchStatement] | FunctorIdentity IdFunctor IdCat deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    
    -- * Types for cone
    
    -- | A cone identifier is a String.
    type IdCone = String
    -- | A cone statement is a mapping of the underlying natural transformation.
    data ConeStatement = MapCone IdObject Composition deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    -- | A cone declaration is a cone created from scratch.
    data ConeDeclaration = ConeDeclaration IdCone IdObject IdFunctor [ConeStatement] deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    -- * Types for cocone
    
    -- | A cocone identifier is a String.
    type IdCocone = String
    -- | A cocone statement is a mapping of the underlying natural transformation.
    data CoconeStatement = MapCocone IdObject Composition deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    -- | A cocone declaration is a cocone created from scratch.
    data CoconeDeclaration = CoconeDeclaration IdCocone IdFunctor IdObject [CoconeStatement] deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    -- * Types for exponential objects
    
    -- | An exponential identifier is a String.
    type IdExp = String
    -- | An exponential declaration is an exponential created from scratch.
    data ExponentialDeclaration = ExponentialDeclaration IdExp IdCat IdObject Composition Composition Composition deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    -- * Types for sketches
    
    -- | A sketch identifier is a String.
    type IdSketch = String
    -- | A sketch statement is a cone, a cocone or an exponential constraint.
    data SketchStatement = ConeConstraint IdCone | CoconeConstraint IdCocone | ExponentialConstraint IdExp deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    -- | A renaming statement allows to rename objects or arrows of a category.
    data RenamingStatement = RenameArrow IdObject IdArrow IdArrow | RenameObject IdObject IdObject IdObject deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    -- | A sketch declaration is either a sketch created from scratch or a colimit of existing sketches with renaming statements.
    data SketchDeclaration = SketchDeclaration IdSketch IdCat [SketchStatement] | ColimitSketch IdSketch IdFunctor [RenamingStatement] deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    -- * Types for sketch morphisms

    -- | A sketch morphism identifier is a String.
    type IdSmorphism = String
    -- | A sketch morphism declaration is the promotion of a functor to a sketch morphism.
    data SmorphismDeclaration = SmorphismDeclaration IdSmorphism IdSketch IdSketch [FunctorStatement] | SmorphismIdentity IdSmorphism IdSketch | SmorphismInclusion IdSmorphism IdSketch IdSketch deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)

    -- * Types for imports
    
    -- | An imported object.
    data ImportStatement = FromImportAsStatement FilePath Identifier Identifier | ImportHidingStatement FilePath [Identifier] | ImportAsHidingStatement FilePath Identifier [Identifier] deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    -- * Types for models.
    type IdModel = String
    data SupportedIntermediateLanguage = Haskell | Python | C deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    type ForeignCode = String
    
    data ModelDeclaration = ModelDeclaration IdModel IdSketch SupportedIntermediateLanguage ForeignCode deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)

    -- * PARSEC LANGUAGE DEFINITION

    def = emptyDef {
        P.commentStart = "/*",
        P.commentEnd = "*/",
        P.commentLine = "//",
        P.nestedComments = False,
        P.identStart = alphaNum <|> oneOf "_^&|+*/[]@€$£.'\\!\"#%-<>=?`~",
        P.identLetter = alphaNum <|> oneOf "_^&|+*/[]@€$£.'\\!\"#%-<>=?`~",
        P.opStart = oneOf ">-=:,",
        P.opLetter = oneOf ">-=:",
        P.reservedNames = ["category","id","functor","Sketch","cone","cocone","exp","sketch","colimit","smorphism","from","import","as","hiding","Haskell","Python","C","model","end_model"],
        P.reservedOpNames = ["::","<<",">>","->","=>",":",",",":="],
        P.caseSensitive = True
    } :: GenLanguageDef String () Identity


    lexer = P.makeTokenParser def
    lexeme = P.lexeme lexer
    whiteSpace = P.whiteSpace lexer
    symbol = P.symbol lexer
    reserved = P.reserved lexer
    reservedOp = P.reservedOp lexer
    stringLiteral = P.stringLiteral lexer
    colon = P.colon lexer
    semi = P.semi lexer
    dot = P.dot lexer
    parens = P.parens lexer
    braces = P.braces lexer
    brackets = P.brackets lexer
    identifier = P.identifier lexer
    commaSep = P.commaSep lexer

    eol :: Parsec String () Char
    eol = choice [newline, char ';']


    -- * FALCON PARSER

    -- ** Category declaration

    testCategoryDeclaration1 :: String
    testCategoryDeclaration1 = "category LCI    {f : A -> B;;;\nA\n;X;\nY;}"


    testCategoryDeclaration2 :: String
    testCategoryDeclaration2 = "category JolieCategory{g.f => h.i\n;\n\nx>>g>>i => Id\n}"
    
    testCategoryDeclaration3 :: String
    testCategoryDeclaration3 = "category x colimit f{cat.g => h; cat.A -> B}"
    
    testCategoryDeclaration4 :: String
    testCategoryDeclaration4 = "category x colimit f"

    parseObject :: Parsec String () CategoryStatement
    parseObject = Object <$> identifier

    parseArrow :: Parsec String () CategoryStatement
    parseArrow = do
        arrowName <- identifier
        colon
        domain <- identifier
        reservedOp "->"
        codomain <- identifier
        return $ Arrow arrowName domain codomain

    parseDoubleBrackets :: Parsec String () Composition
    parseDoubleBrackets = do
        arrows <- sepBy1 identifier (reservedOp ">>")
        return $ reverse arrows
        
    parseDot :: Parsec String () Composition
    parseDot = do
        arrows <- sepBy1 identifier (reservedOp "<<")
        return arrows

    parseCompositionLeft :: Parsec String () Composition
    parseCompositionLeft = do
        case1 <- lookAhead (try parseDoubleBrackets)
        case2 <- lookAhead (try parseDot)
        if length case1 > length case2 then do
            parseDoubleBrackets
        else do
            parseDot

    parseCompositionRight :: Parsec String () Composition
    parseCompositionRight = ((const $ []) <$> (reserved "id")) <|> parseCompositionLeft


    parseLaw :: Parsec String () CategoryStatement
    parseLaw = do
        leftComp <- parseCompositionLeft
        reservedOp "=>" <|> reservedOp ":="
        rightComp <- parseCompositionRight
        return $ Law leftComp rightComp

    parseBody :: Parsec String () [CategoryStatement]
    parseBody = do
        (many eol)
        sepEndBy ((try parseLaw) <|> (try parseArrow) <|> (try parseObject)) (many eol)    
    
    parseCategoryDeclaration :: Parsec String () CategoryDeclaration
    parseCategoryDeclaration = do
        reserved "category"
        idCat <- identifier
        (many eol)
        statements <- braces parseBody
        return $ CategoryDeclaration idCat statements

    -- ** Functor declaration

    testFunctor1 = "functor funct(C1,C2){ A -> B; two => one.three;}"
    testFunctor2 = "functor funct(C1,Sketch){ A -> B; two => one.three;}"

    parseFunctorArguments :: Parsec String () (Either () IdCat, Either () IdCat)
    parseFunctorArguments = do
        args <- commaSep (try (Right <$> identifier) <|> ((const $ Left ()) <$> reserved "Sketch"))
        if length args /= 2 then do
            fail $ "Two arguments needed for a functor declaration, "++ (show $ length args) ++ " where given." 
        else do
            if null (args !! 0) then do
                fail "Sketch category can't be the domain of a functor." 
            else do
                return (args !! 0, args !! 1)

    parseMapObj :: Parsec String () FunctorStatement
    parseMapObj = do
        x <- identifier
        reservedOp "->"
        y <- identifier
        return $ MapObject x y

    parseMapArrow :: Parsec String () FunctorStatement
    parseMapArrow = do
        x <- identifier
        reservedOp "=>"
        y <- parseCompositionRight
        return $ MapArrow x y

    parseFunctorStatements :: Parsec String () [FunctorStatement]
    parseFunctorStatements = do
        (many eol)
        sepEndBy ((try parseMapObj) <|> (try parseMapArrow)) (many eol)

    parseMapObjSketch :: Parsec String () FunctorToSketchStatement
    parseMapObjSketch = do
        x <- identifier
        reservedOp "->"
        y <- identifier
        return $ MapObjectToSketch x y

    parseMapArrowSketch :: Parsec String () FunctorToSketchStatement
    parseMapArrowSketch = do
        x <- identifier
        reservedOp "=>"
        y <- identifier
        return $ MapArrowToSmorphism x y

    parseFunctorSketchStatements :: Parsec String () [FunctorToSketchStatement]
    parseFunctorSketchStatements = do
        (many eol)
        sepEndBy ((try parseMapObjSketch) <|> (try parseMapArrowSketch)) (many eol)

    parseFunctorDeclaration1 :: Parsec String () FunctorDeclaration
    parseFunctorDeclaration1 = do
        reserved "functor"
        idFunctor <- identifier
        (dom,codom) <- parens parseFunctorArguments
        if null codom then do
            let Right d = dom
            (many eol)
            statements <- braces parseFunctorSketchStatements
            return $ FunctorToSketchDeclaration idFunctor d statements
        else do
            let Right d = dom
            let Right c = codom 
            (many eol)
            statements <- braces parseFunctorStatements 
            return $ FunctorDeclaration idFunctor d c statements 

    parseFunctorDeclaration2 :: Parsec String () FunctorDeclaration
    parseFunctorDeclaration2 = do
        reserved "functor"
        idFunctor <- identifier
        reserved "id"
        dom <- parens identifier
        return $ FunctorIdentity idFunctor dom

    parseFunctorDeclaration :: Parsec String () FunctorDeclaration
    parseFunctorDeclaration = (try parseFunctorDeclaration2) <|> parseFunctorDeclaration1
        
        
        

    -- ** Cone declaration

    testCone1 :: String
    testCone1 = "cone cone1 (apex, funct) {1=>f; 2=>g}"

    parseConeArguments :: Parsec String () (IdObject,IdFunctor)
    parseConeArguments = do
        args <- commaSep identifier
        if length args /= 2 then do
            fail $ "Two arguments needed for a cone declaration, " ++ (show $ length args) ++ " were given."
        else do
            return (args !! 0, args !! 1)

    parseMapCone :: Parsec String () ConeStatement
    parseMapCone = do
        x <- identifier
        reservedOp "=>"
        y <- parseCompositionRight
        return $ MapCone x y

    parseConeStatements :: Parsec String () [ConeStatement]
    parseConeStatements = do
        (many eol)
        sepEndBy parseMapCone (many eol)

    parseConeDeclaration :: Parsec String () ConeDeclaration
    parseConeDeclaration = do
        reserved "cone"
        idCone <- identifier
        (apex, funct) <- parens parseConeArguments
        (many eol)
        statements <- braces parseConeStatements
        return $ ConeDeclaration idCone apex funct statements

    -- ** Cocone declaration

    testCocone1 :: String
    testCocone1 = "cocone cocone1 (funct, nadir) {1=>f; 2=>g}"

    parseCoconeArguments :: Parsec String () (IdFunctor,IdObject)
    parseCoconeArguments = do
        args <- commaSep identifier
        if length args /= 2 then do
            fail $ "Two arguments needed for a cocone declaration, " ++ (show $ length args) ++ " were given."
        else do
            return (args !! 0, args !! 1)

    parseMapCocone :: Parsec String () CoconeStatement
    parseMapCocone = do
        x <- identifier
        reservedOp "=>"
        y <- parseCompositionRight
        return $ MapCocone x y

    parseCoconeStatements :: Parsec String () [CoconeStatement]
    parseCoconeStatements = do
        (many eol)
        sepEndBy parseMapCocone (many eol)

    parseCoconeDeclaration :: Parsec String () CoconeDeclaration
    parseCoconeDeclaration = do
        reserved "cocone"
        idCocone <- identifier
        (funct,nadir) <- parens parseCoconeArguments
        (many eol)
        statements <- braces parseCoconeStatements
        return $ CoconeDeclaration idCocone funct nadir statements

    -- ** Exponential object declaration

    testExponentialObject1 = "exp objExp(sk,apex,legPowerObject,legInnerDomain,eval)"

    parseExpArguments :: Parsec String () (IdCat, IdObject, Composition, Composition, Composition)
    parseExpArguments = do
        idCat <- identifier
        reservedOp ","
        idObj <- identifier
        reservedOp ","
        args <- commaSep parseCompositionRight
        if length args /= 3 then do
            fail $ "Five arguments needed for an exponential object declaration, " ++ (show $ (length args) + 2) ++ " were given."
        else do
            return (idCat, idObj, args !! 0, args !! 1, args !! 2)

    parseExponentialDeclaration :: Parsec String () ExponentialDeclaration
    parseExponentialDeclaration = do
        reserved "exp"
        idExp <- identifier
        (idCat,idObj,legPowerObj,legInnerDomain,eval) <- parens parseExpArguments
        return $ ExponentialDeclaration idExp idCat idObj legPowerObj legInnerDomain eval

    -- ** Sketch declaration

    testSketchDeclaration1 :: String
    testSketchDeclaration1 = "sketch LCI(cat){cone c1;cocone cc1;exp e;}"

    testSketchDeclaration2 :: String
    testSketchDeclaration2 = "sketch s colimit f"
    
    testSketchDeclaration3 :: String
    testSketchDeclaration3 = "sketch s colimit f{i1.A -> B;i2.f => g}"

    parseRenameObject :: Parsec String () RenamingStatement
    parseRenameObject = do
        idObject <- identifier
        reservedOp "::"
        idObject1 <- identifier
        reservedOp "->"
        idObject2 <- identifier
        return $ RenameObject idObject idObject1 idObject2
        
    parseRenameArrow :: Parsec String () RenamingStatement
    parseRenameArrow = do
        idObject <- identifier
        reservedOp "::"
        idArrow1 <- identifier
        reservedOp "=>"
        idArrow2 <- identifier
        return $ RenameArrow idObject idArrow1 idArrow2

    parseRenamingBody :: Parsec String () [RenamingStatement]
    parseRenamingBody = do
        (many eol)
        sepEndBy ((try parseRenameObject) <|> (try parseRenameArrow)) (many eol)

    parseSketchCone :: Parsec String () SketchStatement
    parseSketchCone = do
        reserved "cone"
        idCone <- identifier
        return $ ConeConstraint idCone
        
    parseSketchCocone :: Parsec String () SketchStatement
    parseSketchCocone = do
        reserved "cocone"
        idCocone <- identifier
        return $ CoconeConstraint idCocone
        
    parseSketchExp :: Parsec String () SketchStatement
    parseSketchExp = do
        reserved "exp"
        idExp <- identifier
        return $ ExponentialConstraint idExp

    parseSketchBody :: Parsec String () [SketchStatement]
    parseSketchBody = do
        (many eol)
        sepEndBy ((try parseSketchExp) <|> (try parseSketchCocone) <|> (try parseSketchCone)) (many eol)

    parseUsualSketch :: Parsec String () SketchDeclaration
    parseUsualSketch = do
        idSketch <- identifier
        idCat <- parens identifier
        (many eol)
        statements <- braces parseSketchBody
        return $ SketchDeclaration idSketch idCat statements

    parseColimitSketch :: Parsec String () SketchDeclaration
    parseColimitSketch = do
        idSketch <- identifier
        reserved "colimit"
        idFunct <- identifier
        (many eol)
        renamingStatements <- braces parseRenamingBody
        return $ ColimitSketch idSketch idFunct renamingStatements
        
    parseColimitSketchWithoutRenaming :: Parsec String () SketchDeclaration
    parseColimitSketchWithoutRenaming = do
        idSketch <- identifier
        reserved "colimit"
        idFunct <- identifier
        return $ ColimitSketch idSketch idFunct []

    parseSketchDeclaration :: Parsec String () SketchDeclaration
    parseSketchDeclaration = do
        reserved "sketch"
        (try parseColimitSketch) <|> (try parseColimitSketchWithoutRenaming) <|> (try parseUsualSketch)

    -- ** Sketch morphism declaration

    testSketchMorphism1 = "smorphism sm1(sk1,sk2){f => g; a -> b;}"

    parseSketchMorphismArguments1 :: Parsec String () (IdSketch,IdSketch)
    parseSketchMorphismArguments1 = do
        args <- commaSep identifier
        if length args /= 2 then do
            fail $ "Two arguments needed for a sketch morphism declaration, " ++ (show $ length args) ++ " were given."
        else do
            return (args !! 0, args !! 1)

    parseSketchMorphismDeclaration1 :: Parsec String () SmorphismDeclaration
    parseSketchMorphismDeclaration1 = do
        reserved "smorphism"
        idSMorphism <- identifier
        (dom,codom) <- parens parseSketchMorphismArguments1
        (many eol)
        statements <- braces parseFunctorStatements 
        return $ SmorphismDeclaration idSMorphism dom codom statements
    
    parseSketchMorphismDeclaration2 :: Parsec String () SmorphismDeclaration
    parseSketchMorphismDeclaration2 = do
        reserved "smorphism"
        idSMorphism <- identifier
        reserved "id"
        (dom) <- parens identifier
        return $ SmorphismIdentity idSMorphism dom
        
    parseSketchMorphismDeclaration3 :: Parsec String () SmorphismDeclaration
    parseSketchMorphismDeclaration3 = do
        reserved "smorphism"
        idSMorphism <- identifier
        reserved "inclusion"
        (dom,codom) <- parens parseSketchMorphismArguments1
        return $ SmorphismInclusion idSMorphism dom codom

    parseSketchMorphismDeclaration :: Parsec String () SmorphismDeclaration
    parseSketchMorphismDeclaration = (try parseSketchMorphismDeclaration3) <|> (try parseSketchMorphismDeclaration2) <|> parseSketchMorphismDeclaration1
        

    -- ** Import statement

    testImport1 :: String
    testImport1 = "from \"test.fc\" import test as Test "
    
    testImport2 :: String
    testImport2 = "from \"test.fc\" import test"
    
    testImport3 :: String
    testImport3 = "import \"test.fc\""
    
    testImport4 :: String
    testImport4 = "import \"test.fc\" hiding (a,b,c)"
    
    testImport5 :: String
    testImport5 = "import \"test.fc\" as f hiding (a,b,c)"
    
    testImport6 :: String
    testImport6 = "import \"test.fc\" as f"

    parseFromImportAsStatement :: Parsec String () ImportStatement
    parseFromImportAsStatement = do
        reserved "from"
        filename <- stringLiteral
        reserved "import"
        idImported <- identifier
        reserved "as"
        newId <- identifier;   
        return $ FromImportAsStatement filename idImported newId
        
    parseFromImportStatement :: Parsec String () ImportStatement
    parseFromImportStatement = do
        reserved "from"
        filename <- stringLiteral
        reserved "import"
        idImported <- identifier  
        return $ FromImportAsStatement filename idImported idImported
    
    parsePureImportStatement :: Parsec String () ImportStatement
    parsePureImportStatement = do
        reserved "import"
        filename <- stringLiteral 
        return $ ImportHidingStatement filename []
        
    parseImportAsStatement :: Parsec String () ImportStatement
    parseImportAsStatement = do
        reserved "import"
        filename <- stringLiteral 
        reserved "as"
        idAs <- identifier
        return $ ImportAsHidingStatement filename idAs []
        
    parsePureImportHidingStatement :: Parsec String () ImportStatement
    parsePureImportHidingStatement = do
        reserved "import"
        filename <- stringLiteral 
        reserved "hiding"
        hidingIds <- parens $ commaSep identifier
        return $ ImportHidingStatement filename hidingIds
        
    parsePureImportAsHidingStatement :: Parsec String () ImportStatement
    parsePureImportAsHidingStatement = do
        reserved "import"
        filename <- stringLiteral 
        reserved "as"
        idAs <- identifier
        reserved "hiding"
        hidingIds <- parens $ commaSep identifier
        return $ ImportAsHidingStatement filename idAs hidingIds
        
    parseImportStatement :: Parsec String () ImportStatement
    parseImportStatement = (try parseFromImportAsStatement) <|> (try parseFromImportStatement) <|> (try parsePureImportAsHidingStatement) <|> (try parsePureImportHidingStatement) <|> (try parseImportAsStatement) <|> parsePureImportStatement


    -- ** Model parsers
    
    testModel1 :: String
    testModel1 = "model m1(sk1,Haskell)\ndata Test = Test\nend_model"
    
    parseIntermediateLanguage :: Parsec String () SupportedIntermediateLanguage
    parseIntermediateLanguage = (try $ (const Haskell) <$> reserved "Haskell") <|> (try $ (const Python) <$> reserved "Python") <|> (try $ (const C) <$> reserved "C")
    
    parseModelArguments :: Parsec String () (IdSketch, SupportedIntermediateLanguage)
    parseModelArguments = do
        idSketch <- identifier
        reservedOp ","
        lang <- parseIntermediateLanguage
        return (idSketch, lang)
    
    parseModelDeclaration :: Parsec String () ModelDeclaration
    parseModelDeclaration = do
        reserved "model"
        idModel <- identifier
        (idSketch, lang) <- parens parseModelArguments
        foreignCode <- manyTill anyChar (try (reserved "end_model"))
        return $ ModelDeclaration idModel idSketch lang foreignCode
        

    -- ** Falcon file parser

    -- | A list of parsed Falcon tokens read from a file.
    type ParsedFalconFile = [FalconToken]

    -- | Parser for a Falcon program.
    parseFalcon :: Parsec String () ParsedFalconFile
    parseFalcon = do
        whiteSpace
        many eol
        r <- sepEndBy ((Import <$> parseImportStatement) <|> (Category <$> parseCategoryDeclaration) <|> (Sketch <$> parseSketchDeclaration) <|> (Functor <$> parseFunctorDeclaration) <|> (Cone <$> parseConeDeclaration) <|> (Cocone <$> parseCoconeDeclaration) <|> (Exponential <$> parseExponentialDeclaration) <|> (Smorphism <$> parseSketchMorphismDeclaration) <|> (Model <$> parseModelDeclaration)) (many eol)
        eof
        return $ r


    testFalcon1 :: String
    testFalcon1 = "from \"test.fc\" import test as test;category cat{A;B;C;f : A -> B;};functor f(cat,cat){f => f;C -> C;};cone c1(f,C){A => f; B => f;};"
