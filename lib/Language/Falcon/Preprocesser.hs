{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

{-| Module  : falcon
Description : Preprocesser for falcon.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Preprocess falcon files before the semantic analysis. Handles imports.

-}

module Language.Falcon.Preprocesser
(
    PreprocessError(..),
    PreprocessedProgram(..),
    preprocessSeparator,
    preprocessFile,
    parseFalconFile,
    identifierToToken,
)

where
    import              Language.Falcon.Parser  as P
    
    import              Text.Parsec                 (ParseError, parse)
    
    import              Math.IO.PrettyPrint
    
    import              Data.List                   (sortBy)
    import              Data.WeakSet                (Set)
    import qualified    Data.WeakSet            as  Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap                (Map)
    import qualified    Data.WeakMap            as  Map
    import              Data.WeakMap.Safe
    import              Data.Simplifiable
    import              Data.Maybe                  (fromJust)
    
    import              GHC.Generics
    
    import              Control.Exception
    
    -- | An error occuring during the preprocessing.
    data PreprocessError = CouldNotFindFile FilePath
                         | SeveralObjectsHaveSameIdentifier FalconToken FalconToken
                         | ParseError FilePath ParseError
                        deriving (Show, Eq, Generic)
    
    -- | A mapping from identifier to 'FalconToken's.
    type PreprocessedProgram =  Map Identifier FalconToken
    
    preprocessSeparator :: String
    preprocessSeparator = "::"
    
    -- | Return the identifier of a 'FalconToken' as a String.
    identifier :: FalconToken -> Maybe String
    identifier (Import (FromImportAsStatement _ _ name)) = Just name
    identifier (Import (ImportHidingStatement _ _)) = Nothing
    identifier (Import (ImportAsHidingStatement _ _ _)) = Nothing
    identifier (Category (CategoryDeclaration name _)) = Just name
    identifier (Sketch (SketchDeclaration name _ _)) = Just name
    identifier (Sketch (ColimitSketch name _ _)) = Just name
    identifier (Functor (FunctorDeclaration name _ _ _)) = Just name
    identifier (Functor (FunctorToSketchDeclaration name _ _)) = Just name
    identifier (Functor (FunctorIdentity name _)) = Just name
    identifier (Cone (ConeDeclaration name _ _ _)) = Just name
    identifier (Cocone (CoconeDeclaration name _ _ _)) = Just name
    identifier (Exponential (ExponentialDeclaration name _ _ _ _ _)) = Just name
    identifier (Smorphism (SmorphismDeclaration name _ _ _)) =  Just name
    identifier (Smorphism (SmorphismIdentity name _)) =  Just name
    identifier (Smorphism (SmorphismInclusion name _ _)) =  Just name
    identifier (Model (ModelDeclaration name _ _ _)) =  Just name
          
    -- | From a 'ParsedFalconFile' get all identifiers defined in the file.
    getAllIdentifiersInFile :: ParsedFalconFile -> [Identifier]
    getAllIdentifiersInFile [] = []
    getAllIdentifiersInFile (token : xs)
        | null $ identifier token = getAllIdentifiersInFile xs
        | otherwise = i : (getAllIdentifiersInFile xs)
        where
            Just i = identifier token
    
    replace :: Char -> String -> String -> String
    replace c r (x:xs)
        | c == x = r ++ (replace c r xs)
        | otherwise = x:(replace c r xs)
    replace _ _ [] = []
    
    -- | Parse a Falcon file.
    parseFalconFile :: FilePath -> IO (Either PreprocessError ParsedFalconFile)
    parseFalconFile fp = do
        fileOrError <- (try $ readFile fp) :: IO (Either SomeException String)
        if null fileOrError then return $ Left $ CouldNotFindFile fp else do 
            let Right file = fileOrError
            let parsedTokensOrError = parse parseFalcon "" file
            if null parsedTokensOrError then do 
                let Left err = parsedTokensOrError
                return $ Left $ ParseError fp err
            else do
                let Right parsedTokens = parsedTokensOrError
                let duplicate_identifiers = duplicates parsedTokens identifier
                if duplicate_identifiers /= []
                then do
                    return $ Left (SeveralObjectsHaveSameIdentifier (fst.head $ duplicate_identifiers) (snd.head $ duplicate_identifiers))
                else do
                    return $ Right parsedTokens
    
    
    -- | Replace 'ImportHidingStatement's and 'ImportAsHidingStatement's by 'FromImportAsStatement's.
    replacePureImportByImportAs :: ParsedFalconFile -> IO (Either PreprocessError ParsedFalconFile)
    replacePureImportByImportAs [] = return $ Right []
    replacePureImportByImportAs ((Import (ImportHidingStatement fp hides)):xs) = do
        errOrParsedFile <- parseFalconFile fp
        if null errOrParsedFile then return errOrParsedFile else do
            let Right file = errOrParsedFile
            let identifiers = filter (\i -> not $ elem i hides) $ getAllIdentifiersInFile file
            errOrRest <- replacePureImportByImportAs xs
            if null errOrRest then return errOrRest else do
                let Right rest = errOrRest
                return $ Right $ ((\i -> Import (FromImportAsStatement fp i i)) <$> identifiers) ++ rest
    replacePureImportByImportAs ((Import (ImportAsHidingStatement fp a hides)):xs) = do
        errOrParsedFile <- parseFalconFile fp
        if null errOrParsedFile then return errOrParsedFile else do
            let Right file = errOrParsedFile
            let identifiers = filter (\i -> not $ elem i hides) $ getAllIdentifiersInFile file
            errOrRest <- replacePureImportByImportAs xs
            if null errOrRest then return errOrRest else do
                let Right rest = errOrRest
                return $ Right $ ((\i -> Import (FromImportAsStatement fp i (a++"."++i))) <$> identifiers) ++ rest
    replacePureImportByImportAs (t:xs) = fmap (t:) <$> replacePureImportByImportAs xs
        
    -- | Prepend the filename to the identifier of a FalconToken. 
    prependFileNameToIdentifiers :: FilePath -> FalconToken -> FalconToken
    prependFileNameToIdentifiers fp (Import (FromImportAsStatement fpi old new)) = Import (FromImportAsStatement fpi (fpi++preprocessSeparator++old) (fp++preprocessSeparator++new))
    prependFileNameToIdentifiers fp (Import (ImportHidingStatement _ _)) = error "prependFileNameToIdentifiers of ImportHidingStatement, ImportHidingStatement should have already been replaced by FromImportAsStatement"
    prependFileNameToIdentifiers fp (Import (ImportAsHidingStatement _ _ _)) = error "prependFileNameToIdentifiers of ImportAsHidingStatement, ImportAsHidingStatement should have already been replaced by FromImportAsStatement"
    prependFileNameToIdentifiers fp (Category (CategoryDeclaration name st)) = Category (CategoryDeclaration (fp++preprocessSeparator++name) st)
    prependFileNameToIdentifiers fp (Sketch (SketchDeclaration name uc st)) = Sketch (SketchDeclaration (fp++preprocessSeparator++name) (fp++preprocessSeparator++uc) (prependFileNameToIdentifiersSketchStatement <$> st))
        where
            prependFileNameToIdentifiersSketchStatement (ConeConstraint name) = (ConeConstraint (fp++preprocessSeparator++name))
            prependFileNameToIdentifiersSketchStatement (CoconeConstraint name) = (CoconeConstraint (fp++preprocessSeparator++name))
            prependFileNameToIdentifiersSketchStatement (ExponentialConstraint name) = (ExponentialConstraint (fp++preprocessSeparator++name))
    prependFileNameToIdentifiers fp (Sketch (ColimitSketch name f st)) = Sketch (ColimitSketch (fp++preprocessSeparator++name) (fp++preprocessSeparator++f) st)
    prependFileNameToIdentifiers fp (Functor (FunctorDeclaration name s t st)) = Functor (FunctorDeclaration (fp++preprocessSeparator++name) (fp++preprocessSeparator++s) (fp++preprocessSeparator++t) st)
    prependFileNameToIdentifiers fp (Functor (FunctorToSketchDeclaration name s st)) = Functor (FunctorToSketchDeclaration (fp++preprocessSeparator++name) (fp++preprocessSeparator++s) (prependFileNameToIdentifiersFunctorToSketchStatement <$> st))
        where
            prependFileNameToIdentifiersFunctorToSketchStatement (MapObjectToSketch idObject idSketch) = MapObjectToSketch idObject (fp++preprocessSeparator++idSketch)
            prependFileNameToIdentifiersFunctorToSketchStatement (MapArrowToSmorphism idArrow idSmorphism) = MapArrowToSmorphism idArrow (fp++preprocessSeparator++idSmorphism)
    prependFileNameToIdentifiers fp (Functor (FunctorIdentity name s)) = Functor (FunctorIdentity (fp++preprocessSeparator++name) (fp++preprocessSeparator++s))
    prependFileNameToIdentifiers fp (Cone (ConeDeclaration name a b st)) = Cone (ConeDeclaration (fp++preprocessSeparator++name) a (fp++preprocessSeparator++b) st)
    prependFileNameToIdentifiers fp (Cocone (CoconeDeclaration name n b st)) = Cocone (CoconeDeclaration (fp++preprocessSeparator++name) n (fp++preprocessSeparator++b) st)
    prependFileNameToIdentifiers fp (Exponential (ExponentialDeclaration name c a po d e)) = Exponential (ExponentialDeclaration (fp++preprocessSeparator++name) (fp++preprocessSeparator++c) a po d e)
    prependFileNameToIdentifiers fp (Smorphism (SmorphismDeclaration name s t sts)) = Smorphism (SmorphismDeclaration (fp++preprocessSeparator++name) (fp++preprocessSeparator++s) (fp++preprocessSeparator++t) sts)
    prependFileNameToIdentifiers fp (Smorphism (SmorphismIdentity name s)) = Smorphism (SmorphismIdentity (fp++preprocessSeparator++name) (fp++preprocessSeparator++s))
    prependFileNameToIdentifiers fp (Smorphism (SmorphismInclusion name s t)) = Smorphism (SmorphismInclusion (fp++preprocessSeparator++name) (fp++preprocessSeparator++s) (fp++preprocessSeparator++t))
    prependFileNameToIdentifiers fp (Model (ModelDeclaration name sk lang code)) = Model (ModelDeclaration (fp++preprocessSeparator++name) (fp++preprocessSeparator++sk) lang code)
    
    -- | Map a function recursively on all identifiers of a 'FalconToken'.
    mapOnIdentifiers :: (Identifier -> Identifier) -> FalconToken -> FalconToken
    mapOnIdentifiers f (Import (FromImportAsStatement fi old new)) = (Import (FromImportAsStatement fi old new))
    mapOnIdentifiers f (Import (ImportHidingStatement _ _)) = error "mapOnIdentifiers on ImportHidingStatement, ImportHidingStatement should have already been replaced by FromImportAsStatement"
    mapOnIdentifiers f (Import (ImportAsHidingStatement _ _ _)) = error "mapOnIdentifiers on ImportAsHidingStatement, ImportAsHidingStatement should have already been replaced by FromImportAsStatement"
    mapOnIdentifiers f (Category (CategoryDeclaration name st)) = Category (CategoryDeclaration (f name) st)
    mapOnIdentifiers f (Sketch (SketchDeclaration name uc st)) = Sketch (SketchDeclaration (f name) (f uc) (mapOnIdentifiersSketchStatement <$> st))
        where
            mapOnIdentifiersSketchStatement (ConeConstraint name) = (ConeConstraint (f name))
            mapOnIdentifiersSketchStatement (CoconeConstraint name) = (CoconeConstraint (f name))
            mapOnIdentifiersSketchStatement (ExponentialConstraint name) = (ExponentialConstraint (f name))
    mapOnIdentifiers f (Sketch (ColimitSketch name f_ st)) = Sketch (ColimitSketch (f name) (f f_) st)
    mapOnIdentifiers f (Functor (FunctorDeclaration name s t st)) = Functor (FunctorDeclaration (f name) (f s) (f t) st)
    mapOnIdentifiers f (Functor (FunctorIdentity name s)) = Functor (FunctorIdentity (f name) (f s))
    mapOnIdentifiers f (Functor (FunctorToSketchDeclaration name s st)) = Functor (FunctorToSketchDeclaration (f name) (f s) (mapOnIdentifiersFunctorToSketchStatement <$> st))
        where
            mapOnIdentifiersFunctorToSketchStatement (MapObjectToSketch idObject idSketch) = MapObjectToSketch idObject (f idSketch)
            mapOnIdentifiersFunctorToSketchStatement (MapArrowToSmorphism idArrow idSmorphism) = MapArrowToSmorphism idArrow (f idSmorphism)
    mapOnIdentifiers f (Cone (ConeDeclaration name a b st)) = Cone (ConeDeclaration (f name) a (f b) st)
    mapOnIdentifiers f (Cocone (CoconeDeclaration name n b st)) = Cocone (CoconeDeclaration (f name) n (f b) st)
    mapOnIdentifiers f (Exponential (ExponentialDeclaration name c a po d e)) = Exponential (ExponentialDeclaration (f name) (f c) a po d e)
    mapOnIdentifiers f (Smorphism (SmorphismDeclaration name s t sts)) = Smorphism (SmorphismDeclaration (f name) (f s) (f t) sts)
    mapOnIdentifiers f (Smorphism (SmorphismIdentity name s)) = Smorphism (SmorphismIdentity (f name) (f s))
    mapOnIdentifiers f (Smorphism (SmorphismInclusion name s t)) = Smorphism (SmorphismInclusion (f name) (f s) (f t))
    mapOnIdentifiers f (Model (ModelDeclaration name sk lang code)) = Model (ModelDeclaration (f name) (f sk) lang code)
    
    -- | Replace identifiers according to a 'FromImportAsStatement'.
    replaceIdentifier :: Identifier -> Identifier -> ParsedFalconFile -> ParsedFalconFile
    replaceIdentifier old new pff = mapOnIdentifiers (\x -> if x == old then new else x) <$> pff
    
    -- | Extracts all 'FromImportAsStatement' which import a given file of a 'ParsedFalconFile'.
    extractFromImportAsStatements :: FilePath -> ParsedFalconFile -> ([FalconToken], ParsedFalconFile)
    extractFromImportAsStatements fp [] = ([], [])
    extractFromImportAsStatements fp (x@(Import (FromImportAsStatement f _ _)):xs)
        | f == fp = (x:a,b)
        | otherwise = (a,x:b)
        where
            (a,b) = extractFromImportAsStatements fp xs
    extractFromImportAsStatements fp (x:xs) = (a,x:b)
        where
            (a,b) = extractFromImportAsStatements fp xs
            
    -- | Find a file to import in a 'ParsedFalconFile' if it can.
    findAFileToImport :: ParsedFalconFile -> Maybe FilePath
    findAFileToImport [] = Nothing
    findAFileToImport (x@(Import (FromImportAsStatement f _ _)):xs) = Just f
    findAFileToImport (x:xs) = findAFileToImport xs
    
    
    
    -- | Imports all necessary files given a blacklist of files already imported.
    importFiles :: [FilePath] -> ParsedFalconFile -> IO (Either PreprocessError ParsedFalconFile)
    importFiles blacklist tokens = do
        let maybeFileToImport = findAFileToImport tokens
        if null maybeFileToImport then return $ Right tokens else do
            let Just fileToImport = maybeFileToImport
            let (importStatements,restOfTokens) = extractFromImportAsStatements fileToImport tokens
            let renamedTokens = foldr (\(Import (FromImportAsStatement _ importedId currentId)) currTokens -> replaceIdentifier currentId importedId currTokens) restOfTokens importStatements
            errOrImportedFile <- parseFalconFile fileToImport
            if null errOrImportedFile then return errOrImportedFile else do
                let Right importedFile = errOrImportedFile
                errOrOnlyAsImports <- replacePureImportByImportAs importedFile
                if null errOrOnlyAsImports then return errOrOnlyAsImports else do
                    let Right onlyAsImports = errOrOnlyAsImports
                    let prependedTokens = prependFileNameToIdentifiers fileToImport <$> onlyAsImports
                    importFiles (fileToImport:blacklist) (renamedTokens++prependedTokens)
                
            
    -- | Return a mapping from each identifier to the token identified by it.
    identifierToToken :: ParsedFalconFile -> PreprocessedProgram
    identifierToToken [] = weakMap []
    identifierToToken (x:xs)
        | null maybeI = error "An import statement remained after the import process."
        | otherwise = Map.insert i x (identifierToToken xs)
        where
            maybeI = identifier x
            Just i = maybeI
    
    -- | Preprocess a Falcon file.
    preprocessFile :: FilePath -> IO (Either PreprocessError PreprocessedProgram)
    preprocessFile fp = fmap identifierToToken <$> preprocessFileHelper fp
    
    -- | Helper of 'preprocessFile'.
    preprocessFileHelper :: FilePath -> IO (Either PreprocessError ParsedFalconFile)
    preprocessFileHelper fp = do
        errOrParsedFile <- parseFalconFile fp
        if null errOrParsedFile then return errOrParsedFile else do
            let Right file = errOrParsedFile
            errOrOnlyAsImports <- replacePureImportByImportAs file
            if null errOrOnlyAsImports then return errOrOnlyAsImports else do
                let Right onlyAsImports = errOrOnlyAsImports
                let prependedTokens = prependFileNameToIdentifiers fp <$> onlyAsImports
                errOrImportDone <- importFiles [fp] prependedTokens
                if null errOrImportDone then return errOrImportDone else do
                    let Right importDone = errOrImportDone
                    let duplicate_identifiers = duplicates (Set.setToList $ set $ importDone) identifier
                    if duplicate_identifiers /= []
                    then do
                        return $ Left (SeveralObjectsHaveSameIdentifier (fst.head $ duplicate_identifiers) (snd.head $ duplicate_identifiers))
                    else do
                        return $ Right importDone
                 

    -- | Take a list l and a function f and return every element x of l such that another element y of l has the same image as x by f.
    duplicates :: (Ord b) => [a] -> (a -> Maybe b) -> [(a,a)]
    duplicates xs f = dupl mapping
        where
            mapping = sortBy (\(x,_) (y,_) -> compare x y) $ zip (f <$> xs) xs
            dupl [] = []
            dupl (_:[]) = []
            dupl (x1:(x2:xs)) = if (not $ null $ fst x1) && (fst x1) == (fst x2) then ((snd x1, snd x2):(dupl (x2:xs))) else dupl (x2:xs)
    

