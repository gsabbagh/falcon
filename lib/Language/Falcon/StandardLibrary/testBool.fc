import "lib/Language/Falcon/StandardLibrary/UsualConstructions/Terminal.fc" as term
import "lib/Language/Falcon/StandardLibrary/Bool.fc" as bool

// Input sketch

category input_cat{
    true_str : Term -> String
    false_str : Term -> String
    putStrLn : String -> IO_ 
}

sketch pre_input(input_cat){}

category instance_shape_input{
    class_term : var_term -> term
    instance_term : var_term -> pre_input
}

smorphism instance_term(term.var,pre_input){
    Term -> Term
}

functor instanciation_input(instance_shape_input,Sketch){
    class_term => term.class
    instance_term => instance_term
}

sketch input colimit instanciation_input

// Output sketch

category pre_output_cat{
    false : Term -> b
    true : Term -> b
    not : b -> b
    not << false => true
    not << true => false
    p1 : b*b -> b
    p2 : b*b -> b
    
    true_str : Term -> String
    false_str : Term -> String
    putStrLn : String -> IO_ 
    
    show : b -> String
    true >> show => true_str
    false >> show => false_str
    
    
    main : Term -> IO_
    main := false >> not >> show >> putStrLn
    
}

sketch pre_output(pre_output_cat){}

category instance_shape_output{
    class_bool : var_bool -> bool.bool
    instance_bool : var_bool -> pre_output
}


smorphism class_bool(bool.var,bool.bool){
    false => false
    true => true
    not => not
    p1 => p1
    p2 => p2
}

smorphism instance_bool(bool.var,pre_output){
    false => false
    true => true
    not => not
    p1 => p1
    p2 => p2
}

functor instanciation_output(instance_shape_output,Sketch){
    class_bool => class_bool
    instance_bool => instance_bool
}

sketch output colimit instanciation_output

smorphism sm(input,output){
    true_str => true_str
    false_str => false_str
    putStrLn => putStrLn 
}

model m(input,Haskell)

type IO_ = IO ()

data Term = Term

true_str = const "True"

false_str = const "False"

end_model