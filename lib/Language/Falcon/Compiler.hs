{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MonadComprehensions #-}

{-| Module  : falcon
Description : Compiler for falcon.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Compiler for falcon.

-}

module Language.Falcon.Compiler
(
    CompileError(..),
    CompileOptions(..),
    parseOptionsCompiler,
    parseOptionsCompilerWithInfo,
    compile,
)

where
    import              Language.Falcon.Parser
    import              Language.Falcon.Preprocesser
    import              Language.Falcon.SemanticAnalyser
    import              Language.Falcon.Precompiler
    import              Language.Falcon.Transcompilers
    
    import              Math.IO.PrettyPrint
    import              Math.Category
    import              Math.Categories.FinSketch hiding (sketchMorphism)
    import              Math.FiniteCategory
    import              Math.FiniteCategories.CompositionGraph

    import              Data.List                   (intercalate)
    import              Data.WeakSet                (Set)
    import qualified    Data.WeakSet            as  Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap                (Map)
    import qualified    Data.WeakMap            as  Map
    import              Data.WeakMap.Safe
    import              Data.Simplifiable
    import              Data.Maybe                  (fromJust)
    
    import              GHC.Generics
    
    import              Options.Applicative
    
    import              System.Directory                            (createDirectoryIfMissing)
    import              System.FilePath.Posix
    import              System.Process                              (callCommand)
    
    import              Data.Version (showVersion)
    import              Paths_falcon (version)
    
    data CompilerError = UnknownModel String String | UnknownSketchMorphism String String | ModelAndSketchMorphismHaveDifferentSource String String | IdentifierIsNotModel String | IdentifierIsNotSketchMorphism String | UnknownMainFunction String  deriving (Show, Eq)
    
    data CompileError = PreprocesserError PreprocessError | SemanticAnalyserError SemanticError | PrecompilerError PrecompileError | CompilerError CompilerError
        deriving (Show, Eq)
    
    data CompileOptions = CompileOptions {
        inputFile  :: FilePath,
        sketchMorphism :: String,
        model :: String,
        sketchMorphismFile :: FilePath,
        modelFile :: FilePath,
        outputFile :: FilePath,
        auxiliaryDirectory :: String,
        transcompileOnly :: Bool,
        analyseSemanticallyOnly :: Bool,
        mainIs :: String,
        verbosity :: Int,
        compilerExecutable :: FilePath,
        additionalOptions :: String
        } deriving (Eq, Show)
    
    parseOptionsCompiler :: Parser CompileOptions
    parseOptionsCompiler = CompileOptions
        <$> argument str
            (  metavar "INPUT_FILE"
            <> help "Input file for the compilation process." )
        <*> argument str
            (  metavar "SKETCH_MORPHISM_IDENTIFIER"
            <> help "Sketch morphism to compile.")
        <*> argument str
            (  help "The identifier of the model to enrich. The type of the model determines the intermediate language Falcon will use."
            <> metavar "MODEL_IDENTIFIER" )
        <*> option str
            (  long "sketch-morphism-file"
            <> help "The file path to the source file containing the sketch morphism to compile. (Neccessary if different from the input file)"
            <> showDefault
            <> value ""
            <> metavar "FILE_PATH" )
        <*> option str
            (  long "model-file"
            <> help "The file path to the source file containing the model to compile. (Neccessary if different from the input file)"
            <> showDefault
            <> value ""
            <> metavar "FILE_PATH" )
        <*> option str
            (  long "out"
            <> short 'o'
            <> help "The file path of the output executable file."
            <> showDefault
            <> value ""
            <> metavar "FILE_PATH" )
        <*> option str
            (  long "auxiliary"
            <> short 'a'
            <> help "The path of the auxiliary directory where intermediate files are written."
            <> showDefault
            <> value "auxiliary"
            <> metavar "DIR" )
        <*> switch
            (  short 't'
            <> long "transcompile-only"
            <> help "Transcompile only; do not compile the resulting file using another compiler.")
        <*> switch
            (  short 's'
            <> long "semantic-only"
            <> help "Only perform the semantic analysis; do not transcompile.")
        <*> option str
            (  long "main-is"
            <> short 'm'
            <> help "Identifier of the main morphism."
            <> showDefault
            <> value "main"
            <> metavar "STR" )
        <*> option auto
            (  long "verbosity"
            <> short 'v'
            <> help "Verbosity of the compilation process."
            <> showDefault
            <> value 1
            <> metavar "INT" )
        <*> option str
            (  long "compiler"
            <> short 'c'
            <> help "Path to the executable of the compiler of the intermediate language."
            <> showDefault
            <> value ""
            <> metavar "FILE_PATH")
        <*> option str
            (  long "options"
            <> help "Additional options to pass to the intermediate compiler."
            <> showDefault
            <> value ""
            <> metavar "STR" )
        
         
    versioner :: Parser (a -> a)
    versioner = infoOption (showVersion version) (long "version" <> help "Show version" <> hidden)
    
    parseOptionsCompilerWithInfo :: ParserInfo CompileOptions
    parseOptionsCompilerWithInfo = info (parseOptionsCompiler <**> helper <**> versioner)( fullDesc
                                                                         <> progDesc "Compile a Falcon program file. The compilation of a model by a sketch morphism is the inverse precomposition functor associated to the sketch morphism applied to the model."
                                                                         <> header "Falcon compiler" )
                                                                         
    
    purgeModels :: PreprocessedProgram -> PreprocessedProgram
    purgeModels wm = Map.filter (not.isModel) wm
        where
            isModel (Model _) = True
            isModel _ = False
    
    createAndWriteFile :: FilePath -> String -> IO ()
    createAndWriteFile path content = do
        createDirectoryIfMissing True $ takeDirectory path
        writeFile path content
    
    isModel (Model _) = True
    isModel _ = False
    
    isSM (Smorphism _) = True
    isSM _ = False
    
    extractDom (Smorphism (SmorphismDeclaration idSMorphism dom codom sts)) = dom
    extractDom (Smorphism (SmorphismIdentity idSmorphism idSketch)) = idSketch
    extractDom (Smorphism (SmorphismInclusion idSmorphism dom codom)) = dom
    
    compile :: CompileOptions -> IO (Maybe CompileError)
    compile options = do
        errOrPreprocessedFile <- preprocessFile (inputFile options)
        if null errOrPreprocessedFile then do
            let Left err = errOrPreprocessedFile
            return $ Just $ PreprocesserError err
        else do
            let Right preprocessedProgram = errOrPreprocessedFile
            if analyseSemanticallyOnly options then do
                let newPreprocessedProgram = purgeModels preprocessedProgram
                let errOrSemanticProgram = analyseSemantically newPreprocessedProgram
                if null errOrSemanticProgram then do
                    let Left err = errOrSemanticProgram
                    return $ Just $ SemanticAnalyserError err
                else do
                    let Right semanticProgram = errOrSemanticProgram
                    putStrLn $ show $ semanticProgram
                    return Nothing
            else do
                let identifierModel = (if modelFile options == "" then inputFile options else modelFile options) ++ preprocessSeparator ++ (model options)
                if null $ preprocessedProgram |?| identifierModel then return $ Just $ CompilerError $ UnknownModel (model options) (if modelFile options == "" then inputFile options else modelFile options) else do
                    let identifierSketchMorphism = (if sketchMorphismFile options == "" then inputFile options else sketchMorphismFile options) ++ preprocessSeparator ++ (sketchMorphism options)
                    if null $ preprocessedProgram |?| identifierSketchMorphism then return $ Just $ CompilerError $ UnknownSketchMorphism (sketchMorphism options) (if sketchMorphismFile options == "" then inputFile options else sketchMorphismFile options)
                    else do
                        if not $ isModel $ preprocessedProgram |!| identifierModel then return $ Just $ CompilerError $ IdentifierIsNotModel (model options)
                        else do
                            if not $ isSM $ preprocessedProgram |!| identifierSketchMorphism then return $ Just $ CompilerError $ IdentifierIsNotSketchMorphism (sketchMorphism options)
                            else do
                                let (Model (ModelDeclaration idModel idSketch lang foreignCode)) = preprocessedProgram |!| identifierModel
                                let domSm = extractDom $ preprocessedProgram |!| identifierSketchMorphism
                                if idSketch /= domSm then return $ Just $ CompilerError $ ModelAndSketchMorphismHaveDifferentSource idSketch domSm
                                else do
                                    let newPreprocessedProgram = purgeModels preprocessedProgram
                                    let errOrSemanticProgram = analyseSemantically newPreprocessedProgram
                                    if null errOrSemanticProgram then do
                                        let Left err = errOrSemanticProgram
                                        return $ Just $ SemanticAnalyserError err
                                    else do
                                        let Right semProg = errOrSemanticProgram
                                        
                                        let maybeSmorphism = semProg |?| identifierSketchMorphism
                                        if null maybeSmorphism then return $ Just $ CompilerError $ UnknownSketchMorphism (sketchMorphism options) (if sketchMorphismFile options == "" then inputFile options else sketchMorphismFile options)
                                        else do
                                            let (Just (SemanticSketchMorphism sm)) = semProg |?| identifierSketchMorphism
                                            let maybeCompilationError = isSketchMorphismCompilable sm
                                            if not $ null maybeCompilationError then do
                                                let (Just err) = maybeCompilationError
                                                return $ Just $ PrecompilerError err
                                            else case lang of
                                                Haskell -> do
                                                    let typesCode = transformObjectToHaskellCode sm <$> ob (underlyingCategory $ target sm)
                                                    let functionsCode = transformMorphismToHaskellCode sm <$> genArrowsWithoutId (underlyingCategory $ target sm)
                                                    let mainCode = if mainIs options == "" then "main = "++haskellFunctionPrefix ++ "main undefined;\n" else "main = "++haskellFunctionPrefix ++ (mainIs options) ++ " undefined;\n"
                                                    let code =  "-- Haskell model\n\n" ++ foreignCode ++ "\n\n-- Falcon generated code\n\n"++ Set.concat typesCode ++ "\n" ++ Set.concat functionsCode ++ "\n\n-- main function\n\n" ++ mainCode
                                                    let fileName = ((auxiliaryDirectory options) </> (takeBaseName (inputFile options)) <.> ".hs")
                                                    createAndWriteFile fileName code
                                                    if transcompileOnly options then return Nothing
                                                    else do
                                                        let executableName = (if outputFile options == "" then (takeBaseName (inputFile options)) <.> ".exe" else outputFile options)
                                                        callCommand $ (if compilerExecutable options == "" then "ghc" else compilerExecutable options)++ " -o " ++ executableName ++ " " ++ (additionalOptions options) ++ " " ++ fileName
                                                        return Nothing
                                                Python -> do
                                                    let functionsCode = transformMorphismToPythonCode sm <$> genArrowsWithoutId (underlyingCategory $ target sm)
                                                    let mainCode = if mainIs options == "" then "if __name__ == \"__main__\":\n" ++ pythonIndent 1 ++ pythonFunctionPrefix ++ "main({})\n" else "if __name__ == \"__main__\":\n" ++ pythonIndent 1 ++ pythonFunctionPrefix ++ mainIs options ++ "({})\n"
                                                    let code =  "# Python model\n\n" ++ foreignCode ++ "\n\n# Falcon generated code\n\n"++ Set.concat functionsCode ++ "\n\n# main function\n\n" ++ mainCode
                                                    let executableName = (if outputFile options == "" then (takeBaseName (inputFile options)) <.> ".py" else outputFile options)
                                                    createAndWriteFile executableName code
                                                    return Nothing
                                                C -> do
                                                    let forwardTypeDeclaration = forwardDefineType sm <$> ob (underlyingCategory $ target sm)
                                                    let forwardFunctionDeclaration = forwardDefineFunction sm <$> genArrowsWithoutId (underlyingCategory $ target sm)
                                                    let typesCode = transformObjectToCCode sm <$> ob (underlyingCategory $ target sm)
                                                    putStrLn $ Set.concat typesCode
                                                    let functionsCode = transformMorphismToCCode sm <$> genArrowsWithoutId (underlyingCategory $ target sm)
                                                    putStrLn $ Set.concat functionsCode
                                                    let mainName = if mainIs options == "" then "main" else mainIs options
                                                    let maybeLabel = getMorphismFromLabel (underlyingCategory $ target sm) mainName
                                                    if null maybeLabel then return $ Just $ CompilerError $ UnknownMainFunction $ mainName else do
                                                        let mainCode = "void main(){\n" ++ cIndent 1 ++ objectToCIdentifier sm (source $ unsafeGetMorphismFromLabel (underlyingCategory $ target sm) mainName) ++ " x = {};\n" ++ cIndent 1 ++ morphismToCIdentifier sm (unsafeGetMorphismFromLabel (underlyingCategory $ target sm) mainName) ++ "(&x);\n}\n"
                                                        let code =  "// C model\n\n" ++ "#include <stdio.h>\n#include <stdlib.h>\n" ++ foreignCode ++ "\n\n// Falcon generated code\n\n"++ Set.concat forwardTypeDeclaration ++ Set.concat typesCode ++ "\n" ++ Set.concat forwardFunctionDeclaration ++ Set.concat functionsCode ++ "\n\n// main function\n\n" ++ mainCode
                                                        let fileName = ((auxiliaryDirectory options) </> (takeBaseName (inputFile options)) <.> ".c")
                                                        createAndWriteFile fileName code
                                                        if transcompileOnly options then return Nothing
                                                        else do
                                                            let executableName = (if outputFile options == "" then (takeBaseName (inputFile options)) <.> ".exe" else outputFile options)
                                                            callCommand $ (if compilerExecutable options == "" then "gcc" else compilerExecutable options)++ " -o " ++ executableName ++ " " ++ (additionalOptions options) ++ " " ++ fileName
                                                            return Nothing
