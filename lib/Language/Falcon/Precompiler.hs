{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MonadComprehensions #-}

{-| Module  : falcon
Description : Precompiler for falcon.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Precompiler for falcon.

-}

module Language.Falcon.Precompiler
(
    PrecompileError(..),
    ObjectRole(..),
    MorphismRole(..),
    ObjectDeterminacy(..),
    MorphismDeterminacy(..),
    getRolesOfObject,
    getRolesOfMorphism,
    getObjectDeterminacy,
    getMorphismDeterminacy,
    isSketchMorphismCompilable,
)

where
    import              Language.Falcon.Parser  as P  hiding (Arrow)
    import              Language.Falcon.SemanticAnalyser
    
    import              Math.Category
    import              Math.FiniteCategory
    import              Math.CartesianClosedCategory
    import              Math.Categories.FinGrph
    import              Math.Categories.CommaCategory
    import              Math.FiniteCategories.One
    import              Math.FiniteCategories.DiscreteTwo
    import              Math.Categories.FunctorCategory
    import              Math.FiniteCategories.CompositionGraph
    import              Math.Categories.ConeCategory
    import              Math.Categories.FinSketch
    import              Math.IO.PrettyPrint

    import              Data.List                   (intercalate)
    import              Data.WeakSet                (Set)
    import qualified    Data.WeakSet            as  Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap                (Map)
    import qualified    Data.WeakMap            as  Map
    import              Data.WeakMap.Safe
    import              Data.Simplifiable
    import              Data.Maybe                  (fromJust)
    
    import              GHC.Generics
    
    data PrecompileError =  WrongObjectDeterminacy String ObjectDeterminacy
                          | WrongMorphismDeterminacy (CGMorphism String String) MorphismDeterminacy
                          | CompositionLawNotDeterminingAnyOfItsConstituents [Arrow String String] [Arrow String String]
                          | TopCompositeLeg (Cone (CompositionGraph String String) (CGMorphism String String) String (CompositionGraph String String) (CGMorphism String String) String) String
                          | BottomCompositeLeg (Cocone (CompositionGraph String String) (CGMorphism String String) String (CompositionGraph String String) (CGMorphism String String) String) String
        deriving (Show, Eq, Generic, Simplifiable, PrettyPrint)
    
    
    -- | The role of an object in the target of a sketch morphism. This role should be unique or be transported by the sketch morphism for the sketch morphism to be compilable.
    data ObjectRole = ImageObject String
                    | ApexObject (Cone (CompositionGraph String String) (CGMorphism String String) String (CompositionGraph String String) (CGMorphism String String) String)
                    | NadirObject (Cocone (CompositionGraph String String) (CGMorphism String String) String (CompositionGraph String String) (CGMorphism String String) String)
                    | PowerObject (Tripod (CompositionGraph String String) (CGMorphism String String) String)
                    deriving (Show, Eq, Generic, Simplifiable, PrettyPrint)
    
    isImageObjectRole :: ObjectRole -> Bool
    isImageObjectRole (ImageObject _) = True
    isImageObjectRole _ = False
    
    -- | The role of a morphism in the target of a sketch morphism. This role should be unique or be transported by the sketch morphism for the sketch morphism to be compilable.    
    data MorphismRole = ImageMorphism (CGMorphism String String)
                      | ConeLeg (Cone (CompositionGraph String String) (CGMorphism String String) String (CompositionGraph String String) (CGMorphism String String) String) String
                      | CoconeLeg (Cocone (CompositionGraph String String) (CGMorphism String String) String (CompositionGraph String String) (CGMorphism String String) String) String
                      | EvalMap (Tripod (CompositionGraph String String) (CGMorphism String String) String)
                      | IdentityMorphism (CGMorphism String String)
                      | CompositeMorphism [CGMorphism String String]
                      | BindingCone (Cone (CompositionGraph String String) (CGMorphism String String) String (CompositionGraph String String) (CGMorphism String String) String)
                      | BindingCocone (Cocone (CompositionGraph String String) (CGMorphism String String) String (CompositionGraph String String) (CGMorphism String String) String)
                      | BindingTripod (Tripod (CompositionGraph String String) (CGMorphism String String) String) (CGMorphism String String) (CGMorphism String String) (CGMorphism String String) -- ^ the target tripod and the source tripod power leg, domain leg and codomain leg
                      | CompositionLawEntry [Arrow String String] [Arrow String String]
                      deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
                      
                      
    isImageMorphismRole :: MorphismRole -> Bool
    isImageMorphismRole (ImageMorphism _) = True
    isImageMorphismRole _ = False
    
    -- | Given a sketch morphism and an object, return the set of roles of this object.
    getRolesOfObject :: SketchMorphism String String -> String -> Set (ObjectRole)
    getRolesOfObject sm obj = imageRoles ||| apexRoles ||| nadirRoles ||| powerRoles
        where
            funct = underlyingFunctor sm
            imageRoles = [ImageObject k | (k,v) <- Map.mapToSet (omap funct), v == obj]
            apexRoles = [ApexObject c | c <- distinguishedCones (target sm), apex c == obj]
            nadirRoles = [NadirObject cc | cc <- distinguishedCocones (target sm), nadir cc == obj]
            powerRoles = [PowerObject t | t <- distinguishedTripods (target sm), powerObject t == obj]
    
    -- | Get the roles of a generating morphism or a generating leg of a (co)cone or any evaluation map.
    getRolesOfMorphism :: SketchMorphism String String -> CGMorphism String String -> Set (MorphismRole)
    getRolesOfMorphism sm morph = imageRoles ||| coneLegRoles ||| coconeLegRoles ||| evalMapRoles ||| identityRoles ||| bindingConeRoles ||| bindingCoconeRoles ||| bindingTripodRoles ||| compositeRoles
        where
            funct = underlyingFunctor sm
            morphismAntecedent m = [k | (k,v) <- Map.mapToSet (mmap funct), v == m]
            imageRoles
                | isIdentity (tgt funct) morph = [ImageMorphism (identity (src funct) k) | (k,v) <- Map.mapToSet (omap funct), v == (source morph)]
                | isGenerator (tgt funct) morph = ImageMorphism <$> morphismAntecedent morph
                | otherwise = ImageMorphism <$> (compose <$> (cartesianProductOfSets $ morphismAntecedent <$> decompose (tgt funct) morph))
            coneLegRoles = [ConeLeg c x | c <- distinguishedCones (target sm), x <- ob (indexingCategoryCone c), (legsCone c) =>$ x == morph]
            coconeLegRoles = [CoconeLeg cc x | cc <- distinguishedCocones (target sm), x <- ob (indexingCategoryCocone cc), (legsCocone cc) =>$ x == morph]
            evalMapRoles = [EvalMap t | t <- distinguishedTripods (target sm), (evalMap t) == morph]
            identityRoles
                | isIdentity (tgt funct) morph = set [IdentityMorphism morph]
                | otherwise = set []
            compositeRoles = if isComposite (tgt funct) morph then set [] else [CompositeMorphism (unsafeArrowToCGMorphism (tgt funct) <$> v) | (k,v) <- Map.mapToSet (law (tgt funct)), k == [unsafeCGMorphismToArrow morph] && all (isMorphismUniquelyDetermined.(getMorphismDeterminacy sm)) (filter (/= morph) (unsafeArrowToCGMorphism (tgt funct) <$> v))] -- special case where f;g;h => i
            topObjects c = [j | j <- ob $ indexingCategoryCone c, Set.null [f | f <- (genArToWithoutId (indexingCategoryCone c) j), Set.null (genArWithoutId (indexingCategoryCone c) (target f) (source f))]]
            bindingConeRoles = [BindingCone c | c <- distinguishedCones (target sm), (apex c == target morph) && null (checkNaturalTransformation $ legsCone $ precomposeConeWithMorphism c morph) && (Set.and [init (decompose (tgt funct) (legsCone (precomposeConeWithMorphism c morph) =>$ i)) /= decompose (tgt funct) (legsCone c =>$ i) | i <- topObjects c]) && (Set.all (\m -> and $ (isMorphismUniquelyDetermined.(getMorphismDeterminacy sm)) <$> (filter (/= morph) (decompose (tgt funct) m))) ((Map.values).components.legsCone $ precomposeConeWithMorphism c morph))]
            bottomObjects cc = [j | j <- ob $ indexingCategoryCocone cc, Set.null [f | f <- (genArFromWithoutId (indexingCategoryCocone cc) j), Set.null (genArWithoutId (indexingCategoryCocone cc) (target f) (source f))]]
            bindingCoconeRoles = [BindingCocone cc | cc <- distinguishedCocones (target sm), (nadir cc == source morph) && null (checkNaturalTransformation $ legsCocone $ postcomposeCoconeWithMorphism cc morph) && (Set.and [ tail (decompose (tgt funct) (legsCocone (postcomposeCoconeWithMorphism cc morph) =>$ i)) /= decompose (tgt funct) (legsCocone cc =>$ i) | i <- bottomObjects cc]) && (Set.all (\m -> and $ (isMorphismUniquelyDetermined.(getMorphismDeterminacy sm)) <$> (filter (/= morph) (decompose (tgt funct) m))) ((Map.values).components.legsCocone $ postcomposeCoconeWithMorphism cc morph))]
            baseTripod t = twoDiagram (universeCategoryTripod t) (internalDomain t) (internalCodomain t)
            bindingTripodRoles = [BindingTripod t (legsCone c =>$ iPow) (legsCone c =>$ iDom) e | t <- distinguishedTripods (target sm), target morph == powerObject t, c <- distinguishedCones (target sm), (cardinal $ ob $ indexingCategoryCone c) == 2 && Set.null (genArrowsWithoutId $ indexingCategoryCone c), iPow <- ob $ indexingCategoryCone c, target (legsCone c =>$ iPow) == (source morph), iDom <- ob $ indexingCategoryCone c, target (legsCone c =>$ iDom) == (internalDomain t), e <- genAr (tgt funct) (apex c) (internalCodomain t), (isMorphismUniquelyDetermined.(getMorphismDeterminacy sm) $ e), morphxid <- ar (tgt funct) (apex c) (apex $ twoCone t), (powerObjectLeg t) @ morphxid == morph @ (legsCone c =>$ iPow) && (internalDomainLeg t) @ morphxid == (legsCone c =>$ iDom) && e == (evalMap t) @ morphxid]
    
    -- | An object in the target of a sketch morphism can either be uniquely determined, not enough contrained or too much constrained for the sketch morphism to be compilable.
    data ObjectDeterminacy = UniquelyDeterminedObject ObjectRole | NotConstrainedEnoughObject | TooMuchConstrainedObject (Set ObjectRole) | ImageObjectHasAdditionalConstraints (Set ObjectRole) (Set ObjectRole) deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    isObjectUniquelyDetermined :: ObjectDeterminacy -> Bool
    isObjectUniquelyDetermined (UniquelyDeterminedObject _) = True
    isObjectUniquelyDetermined _ = False
    
    -- | Given an object in the target of a sketch morphism, return the determinacy of the object.
    getObjectDeterminacy :: SketchMorphism String String -> String -> ObjectDeterminacy
    getObjectDeterminacy sm obj
        | Set.null imageRoles && Set.null roles = NotConstrainedEnoughObject
        | Set.null imageRoles && null (tail $ Set.setToList roles) = UniquelyDeterminedObject $ anElement roles
        | Set.null imageRoles = TooMuchConstrainedObject roles
        | not $ null (tail $ Set.setToList imageRoles) = TooMuchConstrainedObject imageRoles
        | cardinal domainRoles == cardinal roles = UniquelyDeterminedObject $ ImageObject antecedent -- if an object is an image, we check if all roles are transported by the sm
        | otherwise = ImageObjectHasAdditionalConstraints domainRoles roles
        where
            funct = underlyingFunctor sm
            roles = getRolesOfObject sm obj
            imageRoles = Set.filter isImageObjectRole roles
            identitySM = identity FinSketch (source sm)
            ImageObject antecedent = anElement imageRoles
            domainRoles = getRolesOfObject identitySM antecedent
    
    -- | A morphism in the target of a sketch morphism can either be uniquely determined, not enough contrained or too much constrained for the sketch morphism to be compilable.
    data MorphismDeterminacy = UniquelyDeterminedMorphism MorphismRole | NotConstrainedEnoughMorphism | TooMuchConstrainedMorphism (Set MorphismRole) | ImageMorphismHasAdditionalConstraints (Set MorphismRole) (Set MorphismRole) deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    isMorphismUniquelyDetermined :: MorphismDeterminacy -> Bool
    isMorphismUniquelyDetermined (UniquelyDeterminedMorphism _) = True
    isMorphismUniquelyDetermined _ = False
    
    -- | Given a morphism in the target of a sketch morphism, return the determinacy of the morphism.
    getMorphismDeterminacy :: SketchMorphism String String -> CGMorphism String String -> MorphismDeterminacy
    getMorphismDeterminacy sm morph
        | Set.null imageRoles && Set.null roles = NotConstrainedEnoughMorphism
        | Set.null imageRoles && null (tail $ Set.setToList roles) = UniquelyDeterminedMorphism $ anElement roles
        | Set.null imageRoles = TooMuchConstrainedMorphism roles
        | not $ null (tail $ Set.setToList imageRoles) = TooMuchConstrainedMorphism imageRoles
        | cardinal domainRoles == cardinal roles = UniquelyDeterminedMorphism $ ImageMorphism antecedent -- if a morphism is an image, we check if all roles are transported by the sm
        | otherwise = ImageMorphismHasAdditionalConstraints domainRoles roles
        where
            roles = getRolesOfMorphism sm morph
            imageRoles = Set.filter isImageMorphismRole roles
            identitySM = identity FinSketch (source sm)
            ImageMorphism antecedent = anElement imageRoles
            domainRoles = getRolesOfMorphism identitySM antecedent
    
    
    
    
    -- | Remove a law entry in a sketch to check that it was essential to uniquely determine a constituent generator. It returns an "inclusion" sketch morphism even though it is malformed (functoriality is not true).
    removeLawEntryFromSketch :: Sketch String String -> ([Arrow String String],[Arrow String String]) -> SketchMorphism String String
    removeLawEntryFromSketch sketch entry = unsafeSketchMorphism sketch new_sketch funct
        where
            uc = underlyingCategory sketch
            new_cl = Map.delete (fst entry) $ law uc
            new_uc = unsafeCompositionGraph (support uc) new_cl
            funct = completeDiagram Diagram{src = uc, tgt = new_uc, omap = memorizeFunction id (ob uc), mmap = memorizeFunction (\m -> unsafeGetMorphismFromLabel new_uc (unsafeGetLabel m)) (genArrowsWithoutId uc)}
            new_dc = [unsafeCommaObject (funct ->$ (apex c)) One (funct <-@<= (legsCone c)) | c <- distinguishedCones sketch]
            new_dcc = [unsafeCommaObject One (funct ->$ (nadir cc)) (funct <-@<= (legsCocone cc)) | cc <- distinguishedCocones sketch]
            new_t = [unsafeTripod (unsafeCommaObject (funct ->$ (apex (twoCone t))) One (funct <-@<= (legsCone (twoCone t)))) (funct ->£ (evalMap t)) | t <- distinguishedTripods sketch]
            new_sketch = unsafeSketch new_uc new_dc new_dcc new_t
    
    -- | Return wether a law entry is essential for a (co)cone integrity.
    isLawEntryEssentialForAConeIntegrity :: Sketch String String -> ([Arrow String String],[Arrow String String]) -> Bool
    isLawEntryEssentialForAConeIntegrity sk entry = not $ null faulty_cone && null faulty_cocone
        where
            new_sk = target $ removeLawEntryFromSketch sk entry
            faulty_cone = Set.sequenceSet [checkNaturalTransformation $ legsCone c | c <- distinguishedCones new_sk]
            faulty_cocone = Set.sequenceSet [checkNaturalTransformation $ legsCocone cc | cc <- distinguishedCocones new_sk]
    
    -- | Return wether a law entry determines a constituent arrow of the raw paths.
    isLawEntryDeterminingAGenerator :: SketchMorphism String String -> ([Arrow String String],[Arrow String String]) -> Bool
    isLawEntryDeterminingAGenerator skm entry = not.null $ wrongMorphisms
        where
            new_skm = (removeLawEntryFromSketch (target skm) entry) @ skm
            morphismsToExamine = unsafeArrowToCGMorphism (underlyingCategory $ target new_skm) <$> (fst entry ++ snd entry)
            morphismsAndTheirDeterminacy =  [(m,getMorphismDeterminacy new_skm m) | m <- morphismsToExamine] 
            wrongMorphisms = filter (snd.(fmap $ not.isMorphismUniquelyDetermined)) morphismsAndTheirDeterminacy
            
            
    
    -- | Return Just a 'PrecompileError' if the sketch morphism is not compilable, Nothing otherwise.
    isSketchMorphismCompilable :: SketchMorphism String String -> Maybe PrecompileError
    isSketchMorphismCompilable sm
        | not $ Set.null wrongObjects = Just $ uncurry WrongObjectDeterminacy (anElement wrongObjects)
        | not $ Set.null wrongMorphisms = Just $ uncurry WrongMorphismDeterminacy (anElement wrongMorphisms)
        | not $ Set.null entriesNotTransportedBySM = Just $ uncurry WrongMorphismDeterminacy (anElement entriesNotTransportedBySM)
        | not $ Set.null compositionLawEntryNotDeterminingTheirConstituents = Just $ uncurry CompositionLawNotDeterminingAnyOfItsConstituents (anElement compositionLawEntryNotDeterminingTheirConstituents)
        | not $ Set.null compositeLegsWithNoDominator = Just $ anElement compositeLegsWithNoDominator
        | not $ Set.null compositeLegsWithNoDominator2 = Just $ anElement compositeLegsWithNoDominator2
        | otherwise = Nothing
        where
            funct = underlyingFunctor sm
            cat = tgt funct
            objectsToExamine = ob cat
            objectsAndTheirDeterminacy =  [(o,getObjectDeterminacy sm o) | o <- objectsToExamine] 
            wrongObjects = Set.filter (snd.(fmap $ not.isObjectUniquelyDetermined)) objectsAndTheirDeterminacy
            morphismsToExamine = genArrowsWithoutId cat ||| [m | c <- distinguishedCones (target sm), m <- Map.values $ components $ legsCone c, not $ isComposite cat m] ||| [m | cc <- distinguishedCocones (target sm), m <- Map.values $ components $ legsCocone cc, not $ isComposite cat m] ||| [evalMap t | t <- distinguishedTripods (target sm)]
            morphismsAndTheirDeterminacy =  [(m,getMorphismDeterminacy sm m) | m <- morphismsToExamine] 
            wrongMorphisms = Set.filter (snd.(fmap $ not.isMorphismUniquelyDetermined)) morphismsAndTheirDeterminacy
            
            compositionLawEntries = [(entry,entry) | entry <- Map.mapToSet (law cat)]
            compositionLawEntriesWithMorphisms = [(entry,(unsafeArrowToCGMorphism cat <$> rp1, unsafeArrowToCGMorphism cat <$> rp2)) | (entry,(rp1,rp2)) <- compositionLawEntries]
            determinacyLawEntries = [(entry,(getMorphismDeterminacy sm <$> mp1, getMorphismDeterminacy sm <$> mp2)) | (entry,(mp1, mp2)) <- compositionLawEntriesWithMorphisms]
            extractRole (UniquelyDeterminedMorphism r) = r
            roleLawEntires = [(entry, (extractRole <$> dp1, extractRole <$> dp2)) | (entry,(dp1,dp2)) <- determinacyLawEntries]
            onlyImageLawEntries = [(entry,(rp1,rp2)) | (entry,(rp1, rp2)) <- roleLawEntires, all isImageMorphismRole rp1 && all isImageMorphismRole rp2]
            extractImageLaw (ImageMorphism m) = m
            domainMorphismsEntries = [(entry,(extractImageLaw <$> rp1, extractImageLaw <$> rp2)) | (entry,(rp1,rp2)) <- onlyImageLawEntries]
            entriesNotTransportedBySM = [(compose $ unsafeArrowToCGMorphism cat <$> snd entry, ImageMorphismHasAdditionalConstraints (set []) $  getRolesOfMorphism sm (compose $ unsafeArrowToCGMorphism cat <$> snd entry) ||| set [(uncurry CompositionLawEntry entry)]) | (entry,(rp1,rp2)) <- domainMorphismsEntries, compose rp1 /= compose rp2]
            
            compositionLawEntriesWithANonImage = [entry | (entry,(rp1, rp2)) <- roleLawEntires, not $ all isImageMorphismRole rp1 && all isImageMorphismRole rp2]
            compositionLawEntriesNotEssentialForCones = Set.filter (not.(isLawEntryEssentialForAConeIntegrity (target sm))) compositionLawEntriesWithANonImage
            compositionLawEntryNotDeterminingTheirConstituents = Set.filter (not.(isLawEntryDeterminingAGenerator sm)) compositionLawEntriesNotEssentialForCones
            
            compositeLegs = [(c,i,legsCone c =>$ i) | c <- distinguishedCones (target sm), i <- ob (indexingCategoryCone c), isComposite cat (legsCone c =>$ i)]
            compositeLegsWithNoDominator = [TopCompositeLeg c i | (c,i,l) <- compositeLegs, Set.null [m | m <- arTo (indexingCategoryCone c) i, isGenerator cat ((legsCone c) =>$ (source m))]]
            
            compositeLegs2 = [(cc,i,legsCocone cc =>$ i) | cc <- distinguishedCocones (target sm), i <- ob (indexingCategoryCocone cc), isComposite cat (legsCocone cc =>$ i)]
            compositeLegsWithNoDominator2 = [BottomCompositeLeg cc i | (cc,i,l) <- compositeLegs2, Set.null [m | m <- arFrom (indexingCategoryCocone cc) i, isGenerator cat ((legsCocone cc) =>$ (target m))]]
    
