{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MonadComprehensions #-}

{-| Module  : falcon
Description : Transcompiler from Falcon to C.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Transcompiler from Falcon to C.

-}

module Language.Falcon.Transcompilers.C
(
    cIndent,
    cTypePrefix,
    cVarPrefix,
    cFunctionPrefix,
    objectToCIdentifier,
    morphismToCIdentifier,
    forwardDefineType,
    forwardDefineFunction,
    transformObjectToCCode,
    transformMorphismToCCode,
)

where
    import              Language.Falcon.Parser  as P  hiding (Arrow)
    import              Language.Falcon.SemanticAnalyser
    import              Language.Falcon.Precompiler
    
    import              Math.Category
    import              Math.FiniteCategory
    import              Math.CartesianClosedCategory
    import              Math.Categories.FinGrph
    import              Math.Categories.CommaCategory
    import              Math.FiniteCategories.One
    import              Math.FiniteCategories.DiscreteTwo
    import              Math.Categories.FunctorCategory
    import              Math.FiniteCategories.CompositionGraph
    import              Math.Categories.ConeCategory
    import              Math.Categories.FinSketch
    import              Math.IO.PrettyPrint

    import              Data.List                   (intercalate, elemIndex)
    import              Data.WeakSet                (Set)
    import qualified    Data.WeakSet            as  Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap                (Map)
    import qualified    Data.WeakMap            as  Map
    import              Data.WeakMap.Safe
    import              Data.Simplifiable
    import              Data.Maybe                  (fromJust)
    
    import              GHC.Generics
    
    cIndent :: Int -> String
    cIndent 0 = ""
    cIndent x = "    "++(cIndent $ x-1)
    
    cTypePrefix :: String
    cTypePrefix = "Falcon_"
    
    cVarPrefix :: String
    cVarPrefix = "var_"
    
    -- | Transform an object of the target sketch into a C type identifier.
    objectToCIdentifier :: SketchMorphism String String -> String -> String
    objectToCIdentifier sm obj = case role of
        (ImageObject x) -> x
        (ApexObject x) -> cTypePrefix ++ apex x
        (NadirObject x) -> cTypePrefix ++ nadir x
        (PowerObject t) -> cTypePrefix ++ powerObject t
        where
            UniquelyDeterminedObject role = getObjectDeterminacy sm obj
    
    
    -- | Transforms an object of the target of a sketch morphism into C code.
    transformObjectToCCode :: SketchMorphism String String -> String -> String
    transformObjectToCCode sm obj = case role of
        (ImageObject _) -> ";\n"
        (ApexObject c) -> newApex
            where
                b = baseCone c
                topObjects = [i | i <- ob $ indexingCategoryCone c, Set.null [f | f <- (genArToWithoutId (indexingCategoryCone c) i), Set.null (genArWithoutId (indexingCategoryCone c) (target f) (source f))]]
                newApex = "struct "++objectToCIdentifier sm obj++"{\n" ++ (Set.concat [cIndent 1 ++ (objectToCIdentifier sm (baseCone c ->$ x)) ++ "* " ++ cVarPrefix ++ (morphismToCIdentifier sm (legsCone c =>$ x)) ++ ";\n" | x <- topObjects])++"};\n\n"
        (NadirObject cc) -> newNadir
            where
                b = baseCocone cc
                bottomObjects = [i | i <- ob $ indexingCategoryCocone cc, Set.null [f | f <- (genArFromWithoutId (indexingCategoryCocone cc) i), Set.null (genArWithoutId (indexingCategoryCocone cc) (target f) (source f))]]
                newNadir = "struct " ++ objectToCIdentifier sm obj ++ "{\n" ++ (Set.concat [cIndent 1 ++ (objectToCIdentifier sm (baseCocone cc ->$ x)) ++ "* " ++ cVarPrefix ++ (morphismToCIdentifier sm (legsCocone cc =>$ x)) ++ ";\n" | x <- bottomObjects])++"\n"++ cIndent 1 ++ "int current_type;\n}\n\n"
        (PowerObject t) -> "struct "++objectToCIdentifier sm obj++"{\n" ++ cIndent 1 ++ objectToCIdentifier sm (internalCodomain t) ++ "* (*uncurried_function_pointer) (void*);\n" ++ cIndent 1 ++  "void* (*pair_constructor)(void*," ++ objectToCIdentifier sm (internalDomain t)++"*);\n"++cIndent 1++"void* first_arg;\n};\n\n"
        where
            UniquelyDeterminedObject role = getObjectDeterminacy sm obj
            
            
    forwardDefineType :: SketchMorphism String String -> String -> String
    forwardDefineType sm obj = case role of
        (ImageObject x) -> ""
        (ApexObject x) -> "typedef struct " ++ objectToCIdentifier sm obj ++ " " ++ objectToCIdentifier sm obj ++ ";\n"
        (NadirObject x) -> "typedef struct " ++ objectToCIdentifier sm obj ++ " " ++ objectToCIdentifier sm obj ++ ";\n"
        (PowerObject t) -> "typedef struct " ++ objectToCIdentifier sm obj ++ " " ++ objectToCIdentifier sm obj ++ ";\n"
        where
            role = (anElement $ getRolesOfObject sm obj)
            
    cFunctionPrefix :: String
    cFunctionPrefix = "falcon_"
         
    -- | Transform a non identity generating morphism of the target of a sketch morphism into a C identifier.
    morphismToCIdentifier :: SketchMorphism String String -> CGMorphism String String -> String
    morphismToCIdentifier sm morph
        | isComposite (underlyingCategory $ target sm) morph = error "morphismToCIdentifier called on a composite morphism : "++ (show morph)
        | otherwise = case role of
            (ImageMorphism x) -> unsafeGetLabel x
            (ConeLeg c i) -> cFunctionPrefix ++ (unsafeGetLabel morph)
            (CoconeLeg cc i) -> cFunctionPrefix ++ (unsafeGetLabel morph)
            (EvalMap t) -> cFunctionPrefix ++ (unsafeGetLabel morph)
            (IdentityMorphism m) -> error $ "identity morphism given to morphismToCIdentifier : "++show morph
            (CompositeMorphism decomp) -> cFunctionPrefix ++ (unsafeGetLabel morph)
            (BindingCone c) -> cFunctionPrefix ++ (unsafeGetLabel morph)
            (BindingCocone cc) -> cFunctionPrefix ++ (unsafeGetLabel morph)
            (BindingTripod t powLeg domLeg codomLeg) -> cFunctionPrefix ++ (unsafeGetLabel morph)
            (CompositionLawEntry rp1 rp2) -> error "CompositionLawEntry in morphismToCIdentifier"
            where
                UniquelyDeterminedMorphism role = getMorphismDeterminacy sm morph
            
    forwardDefineFunction :: SketchMorphism String String -> CGMorphism String String -> String
    forwardDefineFunction sm morph = case role of
        (ImageMorphism _) -> ";\n"
        (ConeLeg c i) -> objectToCIdentifier sm (target morph) ++ "* " ++ morphismToCIdentifier sm morph ++"("++ objectToCIdentifier sm (source morph) ++ "* x);\n"
        (CoconeLeg cc i) -> objectToCIdentifier sm (target morph) ++ "* " ++ morphismToCIdentifier sm morph ++"("++ objectToCIdentifier sm (source morph) ++ "* x);\n"
        (EvalMap t) -> objectToCIdentifier sm (target morph) ++ "* " ++ morphismToCIdentifier sm morph ++"("++ objectToCIdentifier sm (source morph) ++ "* x);\n"
        (IdentityMorphism m) -> ";\n"
        (CompositeMorphism decomp) -> objectToCIdentifier sm (target morph) ++ "* " ++ morphismToCIdentifier sm morph ++"("++ objectToCIdentifier sm (source morph) ++ "* x);\n"
        (BindingCone c) -> objectToCIdentifier sm (target morph) ++ "* " ++ morphismToCIdentifier sm morph ++"("++ objectToCIdentifier sm (source morph) ++ "* x);\n"
        (BindingCocone cc) -> objectToCIdentifier sm (target morph) ++ "* " ++ morphismToCIdentifier sm morph ++"("++ objectToCIdentifier sm (source morph) ++ "* x);\n"
        (BindingTripod t powLeg domLeg codomLeg) -> objectToCIdentifier sm (source powLeg) ++ "* pair_constructor_" ++ morphismToCIdentifier sm morph ++ "(" ++ objectToCIdentifier sm (target powLeg) ++ "* x, " ++ objectToCIdentifier sm (target domLeg) ++ "* y);\n" ++ objectToCIdentifier sm (target morph) ++ "* " ++ morphismToCIdentifier sm morph ++"("++ objectToCIdentifier sm (source morph) ++ "* x);\n"
        (CompositionLawEntry rp1 rp2) -> error "CompositionLawEntry in forwardDefineFunction"
        where
            UniquelyDeterminedMorphism role = getMorphismDeterminacy sm morph
          
    -- | Transforms a generating morphism of the target of a sketch morphism into C code.
    transformMorphismToCCode :: SketchMorphism String String -> CGMorphism String String -> String
    transformMorphismToCCode sm morph = case role of
        (ImageMorphism _) -> ";\n"
        (ConeLeg c i) -> objectToCIdentifier sm (target morph) ++ "* " ++ morphismToCIdentifier sm morph ++"("++ objectToCIdentifier sm (source morph) ++ "* x){ return x->" ++ cVarPrefix ++ morphismToCIdentifier sm morph ++ ";}\n\n"
        (CoconeLeg cc i) -> objectToCIdentifier sm (target morph) ++ "* " ++ morphismToCIdentifier sm morph ++"("++ objectToCIdentifier sm (source morph) ++ "* x){\n"++ cIndent 1 ++ objectToCIdentifier sm (target morph) ++ "* y = malloc(sizeof(" ++ objectToCIdentifier sm (target morph) ++ "));\n" ++ cIndent 1 ++ "y->" ++ cVarPrefix ++ morphismToCIdentifier sm morph ++ " = x;\n" ++ cIndent 1 ++ "y->current_type = " ++ (show $ fromJust $ elemIndex i $ Set.setToList $ ob $ indexingCategoryCocone cc) ++ ";\n" ++ cIndent 1 ++ "return y;\n}\n\n"
            where 
                fromJust (Just x) = x
        (EvalMap t) -> objectToCIdentifier sm (target morph) ++ "* " ++ morphismToCIdentifier sm morph ++"("++ objectToCIdentifier sm (source morph) ++ "* x){\n"++ cIndent 1 ++ objectToCIdentifier sm (target morph) ++"* r = malloc(sizeof(" ++ objectToCIdentifier sm (target morph) ++ "));\n" ++ cIndent 1 ++ "void* pair = (x->"++ cVarPrefix ++ morphismToCIdentifier sm (powerObjectLeg t)  ++ "->pair_constructor)(x->"++ cVarPrefix ++ morphismToCIdentifier sm (powerObjectLeg t) ++ "->first_arg,x->"++ cVarPrefix ++ morphismToCIdentifier sm (internalDomainLeg t) ++");\n" ++ cIndent 1 ++ "r = (x->"++ cVarPrefix ++ morphismToCIdentifier sm (powerObjectLeg t) ++ "->uncurried_function_pointer)(pair);\n" ++ cIndent 1 ++ "return r;\n}\n\n"
        (IdentityMorphism m) -> ";\n"
        (CompositeMorphism decomp) -> objectToCIdentifier sm (target morph) ++ "* " ++ morphismToCIdentifier sm morph ++"("++ objectToCIdentifier sm (source morph) ++ "* x){\n"++ cIndent 1 ++ "return " ++ concat ((++ "(") <$> (morphismToCIdentifier sm) <$> decomp) ++ "x" ++ replicate (length decomp) ')' ++ ";\n}\n\n"
        (BindingCone c) -> objectToCIdentifier sm (target morph) ++ "* " ++ morphismToCIdentifier sm morph ++"("++ objectToCIdentifier sm (source morph) ++ "* x){\n"++ cIndent 1 ++ objectToCIdentifier sm (target morph) ++ "* y = malloc(sizeof("++ objectToCIdentifier sm (target morph) ++ "));\n" ++ Set.concat [ cIndent 1 ++ "y->" ++ cVarPrefix ++ morphismToCIdentifier sm (legsCone c =>$ i) ++ " = " ++  concat ((++ "(") <$> (morphismToCIdentifier sm) <$> decomp i) ++ "x" ++ replicate (length $ decomp i) ')' ++ ";\n" | i <- topObjects] ++ cIndent 1 ++ "return y;\n}\n\n"
            where
                topObjects = [j | j <- ob $ indexingCategoryCone c, Set.null [f | f <- (genArToWithoutId (indexingCategoryCone c) j), Set.null (genArWithoutId (indexingCategoryCone c) (target f) (source f))]]
                decomp i = decompose (universeCategoryCone c) ((legsCone c) =>$ i @ morph)
        (BindingCocone cc) -> objectToCIdentifier sm (target morph) ++ "* " ++ morphismToCIdentifier sm morph ++"("++ objectToCIdentifier sm (source morph) ++ "* x){\n"++ cIndent 1 ++ "switch (x->current_type) {\n" ++ Set.concat [ cIndent 2 ++ "case " ++ (show $ fromJust $ elemIndex i $ Set.setToList $ ob $ indexingCategoryCocone cc) ++ ":\n" ++ cIndent 3 ++ "return " ++ concat ((++ "(") <$> (morphismToCIdentifier sm) <$> decomp i) ++ "x->" ++ cVarPrefix ++ morphismToCIdentifier sm (legsCocone cc =>$ i) ++ replicate (length $ decomp i) ')' ++ ";\n" ++ cIndent 3 ++ "break;\n" | i <- bottomObjects] ++ cIndent 1 ++ "}\n}\n\n"
            where
                fromJust (Just x) = x
                bottomObjects = [j | j <- ob $ indexingCategoryCocone cc, Set.null [f | f <- (genArToWithoutId (indexingCategoryCocone cc) j), Set.null (genArWithoutId (indexingCategoryCocone cc) (target f) (source f))]]
                decomp i = decompose (universeCategoryCocone cc) (morph @ (legsCocone cc =>$ i))
        (BindingTripod t powLeg domLeg codomLeg) -> objectToCIdentifier sm (source powLeg) ++ "* pair_constructor_" ++ morphismToCIdentifier sm morph ++ "(" ++ objectToCIdentifier sm (target powLeg) ++ "* x, " ++ objectToCIdentifier sm (target domLeg) ++ "* y){\n" ++ cIndent 1 ++ objectToCIdentifier sm (source powLeg) ++ "* r = malloc(sizeof(" ++ objectToCIdentifier sm (source powLeg) ++ "));\n" ++ cIndent 1 ++ "r->" ++ cVarPrefix ++ morphismToCIdentifier sm powLeg ++ " = x;\n" ++ cIndent 1 ++ "r->" ++ cVarPrefix ++ morphismToCIdentifier sm domLeg ++ " = y;\n" ++ cIndent 1 ++ "return r;\n}\n" ++ objectToCIdentifier sm (powerObject t) ++ "* "++  morphismToCIdentifier sm morph ++ "(" ++ objectToCIdentifier sm (internalDomain t) ++ "* x){\n" ++ cIndent 1 ++ objectToCIdentifier sm (powerObject t) ++ "* r = malloc(sizeof(" ++ objectToCIdentifier sm (powerObject t) ++ "));\n" ++ cIndent 1 ++ "r->uncurried_function_pointer = (" ++ objectToCIdentifier sm (target codomLeg) ++ "* (*) (void*)) " ++ morphismToCIdentifier sm codomLeg ++ ";\n" ++ cIndent 1 ++ "r->pair_constructor = (void* (*) (void*," ++ objectToCIdentifier sm (target domLeg) ++ "*)) pair_constructor_" ++ morphismToCIdentifier sm morph ++ ";\n" ++ cIndent 1 ++ "r->first_arg = (void*) x;\n" ++ cIndent 1 ++ "return r;\n}\n\n"   
        (CompositionLawEntry rp1 rp2) -> error "CompositionLawEntry in transformMorphismToCCode"
        where
            UniquelyDeterminedMorphism role = getMorphismDeterminacy sm morph