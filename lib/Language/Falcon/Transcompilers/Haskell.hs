{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MonadComprehensions #-}

{-| Module  : falcon
Description : Transcompiler from Falcon to Haskell.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Transcompiler from Falcon to Haskell.

-}

module Language.Falcon.Transcompilers.Haskell
(
    haskellFunctionPrefix,
    haskellTypePrefix,
    transformObjectToHaskellCode,
    transformMorphismToHaskellCode,
)

where
    import              Language.Falcon.Parser  as P  hiding (Arrow)
    import              Language.Falcon.SemanticAnalyser
    import              Language.Falcon.Precompiler
    
    import              Math.Category
    import              Math.FiniteCategory
    import              Math.CartesianClosedCategory
    import              Math.Categories.FinGrph
    import              Math.Categories.CommaCategory
    import              Math.FiniteCategories.One
    import              Math.FiniteCategories.DiscreteTwo
    import              Math.Categories.FunctorCategory
    import              Math.FiniteCategories.CompositionGraph
    import              Math.Categories.ConeCategory
    import              Math.Categories.FinSketch
    import              Math.IO.PrettyPrint

    import              Data.List                   (intercalate)
    import              Data.WeakSet                (Set)
    import qualified    Data.WeakSet            as  Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap                (Map)
    import qualified    Data.WeakMap            as  Map
    import              Data.WeakMap.Safe
    import              Data.Simplifiable
    import              Data.Maybe                  (fromJust)
    
    import              GHC.Generics
    
    haskellTypePrefix :: String
    haskellTypePrefix = "Falcon_"
    
    -- | Transform an object of the target of a sketch morphism into a Haskell identifier.
    objectToHaskellIdentifier :: SketchMorphism String String -> String -> String
    objectToHaskellIdentifier sm obj = case role of
        (ImageObject x) -> x
        (ApexObject x) -> haskellTypePrefix ++ apex x
        (NadirObject x) -> haskellTypePrefix ++ nadir x
        (PowerObject x) -> haskellTypePrefix ++ powerObject x
        where
            UniquelyDeterminedObject role = getObjectDeterminacy sm obj
    
    -- | Transforms an object of the target of a sketch morphism into Haskell code.
    transformObjectToHaskellCode :: SketchMorphism String String -> String -> String
    transformObjectToHaskellCode sm obj = case role of
        (ImageObject _) -> ";\n"
        (ApexObject c) -> newApex
            where
                b = baseCone c
                topObjects = [i | i <- ob $ indexingCategoryCone c, Set.null [f | f <- (genArToWithoutId (indexingCategoryCone c) i), Set.null (genArWithoutId (indexingCategoryCone c) (target f) (source f))]]
                newApex = "data "++objectToHaskellIdentifier sm obj++" = "++objectToHaskellIdentifier sm obj++" "++ (concat [(objectToHaskellIdentifier sm (b ->$ i))++" " | i <- Set.setToList topObjects])++";\n"
        (NadirObject cc) -> newNadir
            where
                b = baseCocone cc
                bottomObjects = [i | i <- ob $ indexingCategoryCocone cc, Set.null [f | f <- (genArFromWithoutId (indexingCategoryCocone cc) i), Set.null (genArWithoutId (indexingCategoryCocone cc) (target f) (source f))]]
                newNadir = if Set.null bottomObjects then "data "++objectToHaskellIdentifier sm obj else "data "++objectToHaskellIdentifier sm obj++" = "++ (intercalate " | " $ [objectToHaskellIdentifier sm obj++"_"++i++" "++(objectToHaskellIdentifier sm (b ->$ i)) | i <- Set.setToList bottomObjects])++";\n"
        (PowerObject t) -> "type "++objectToHaskellIdentifier sm obj++" = "++(objectToHaskellIdentifier sm (internalDomain t))++" -> "++(objectToHaskellIdentifier sm (internalCodomain t))++";\n"
        where
            UniquelyDeterminedObject role = getObjectDeterminacy sm obj
            
        
    haskellFunctionPrefix :: String
    haskellFunctionPrefix = "falcon_"
            
    -- | Transform a non identity generating morphism of the target of a sketch morphism into a Haskell identifier.
    morphismToHaskellIdentifier :: SketchMorphism String String -> CGMorphism String String -> String
    morphismToHaskellIdentifier sm morph
        | isComposite (underlyingCategory $ target sm) morph = error "morphismToHaskellIdentifier called on a composite morphism : "++ (show morph)
        | otherwise = case role of
            (ImageMorphism x) -> unsafeGetLabel x
            (ConeLeg c i) -> haskellFunctionPrefix ++ (unsafeGetLabel morph)
            (CoconeLeg cc i) -> objectToHaskellIdentifier sm (nadir cc)++"_"++i
            (EvalMap t) -> haskellFunctionPrefix ++ (unsafeGetLabel morph)
            (IdentityMorphism m) -> error $ "identity morphism given to morphismToHaskellIdentifier : "++show morph
            (CompositeMorphism decomp) -> haskellFunctionPrefix ++ (unsafeGetLabel morph)
            (BindingCone c) -> haskellFunctionPrefix ++ (unsafeGetLabel morph)
            (BindingCocone cc) -> haskellFunctionPrefix ++ (unsafeGetLabel morph)
            (BindingTripod t powLeg domLeg codomLeg) -> haskellFunctionPrefix ++ (unsafeGetLabel morph)
            (CompositionLawEntry rp1 rp2) -> error "CompositionLawEntry in morphismToHaskellIdentifier"
            where
                UniquelyDeterminedMorphism role = getMorphismDeterminacy sm morph
            
    -- | Transforms a generating morphism of the target of a sketch morphism into Haskell code.
    transformMorphismToHaskellCode :: SketchMorphism String String -> CGMorphism String String -> String
    transformMorphismToHaskellCode sm morph = case role of
        (ImageMorphism _) -> ";\n"
        (ConeLeg c i) -> morphismToHaskellIdentifier sm morph ++" ("++(objectToHaskellIdentifier sm (apex c))++ Set.concat [if i == j then " x" else " _" | j <- topObjects] ++") = x;\n\n"
            where
                topObjects = [j | j <- ob $ indexingCategoryCone c, Set.null [f | f <- (genArToWithoutId (indexingCategoryCone c) j), Set.null (genArWithoutId (indexingCategoryCone c) (target f) (source f))]]
        (CoconeLeg cc i) -> ";\n\n"
        (EvalMap t) -> morphismToHaskellIdentifier sm morph ++ " (" ++ (objectToHaskellIdentifier sm (apex $ twoCone t)) ++ " function x) = function x;\n\n"
        (IdentityMorphism m) -> ";\n"
        (CompositeMorphism decomp) -> morphismToHaskellIdentifier sm morph ++ " = " ++ intercalate " . " ((morphismToHaskellIdentifier sm) <$> decomp) ++ ";\n\n"
        (BindingCone c) -> morphismToHaskellIdentifier sm morph ++ " x = " ++ objectToHaskellIdentifier sm (apex c) ++ " " ++ intercalate " " (Set.setToList ["(" ++ (intercalate " . " $ morphismToHaskellIdentifier sm <$> (decompose (universeCategoryCone c) ((legsCone c) =>$ i @ morph))) ++ " $ x)" | i <- topObjects]) ++ ";\n\n"
            where
                topObjects = [j | j <- ob $ indexingCategoryCone c, Set.null [f | f <- (genArToWithoutId (indexingCategoryCone c) j), Set.null (genArWithoutId (indexingCategoryCone c) (target f) (source f))]]
        (BindingCocone cc) -> if Set.null bottomObjects then morphismToHaskellIdentifier sm morph ++ " _ = undefined" else Set.concat [morphismToHaskellIdentifier sm morph ++ " (" ++ morphismToHaskellIdentifier sm (legsCocone cc =>$ i) ++ " x) = " ++ intercalate " . " (morphismToHaskellIdentifier sm <$> (decompose (universeCategoryCocone cc) (morph @ (legsCocone cc =>$ i)))) ++ " $ x;\n" | i <- bottomObjects] ++ "\n"
            where
                bottomObjects = [j | j <- ob $ indexingCategoryCocone cc, Set.null [f | f <- (genArToWithoutId (indexingCategoryCocone cc) j), Set.null (genArWithoutId (indexingCategoryCocone cc) (target f) (source f))]]
        (BindingTripod t powLeg domLeg codomLeg) -> morphismToHaskellIdentifier sm morph ++ " x y = " ++ morphismToHaskellIdentifier sm codomLeg ++ " (" ++ objectToHaskellIdentifier sm (source codomLeg) ++ " x y);\n\n"
        (CompositionLawEntry rp1 rp2) -> error "CompositionLawEntry in transformMorphismToHaskellCode"
        where
            UniquelyDeterminedMorphism role = getMorphismDeterminacy sm morph