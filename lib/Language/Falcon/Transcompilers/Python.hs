{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MonadComprehensions #-}

{-| Module  : falcon
Description : Transcompiler from Falcon to Python.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Transcompiler from Falcon to Python.

-}

module Language.Falcon.Transcompilers.Python
(
    pythonIndent,
    pythonFunctionPrefix,
    transformObjectToPythonCode,
    transformMorphismToPythonCode,
)

where
    import              Language.Falcon.Parser  as P  hiding (Arrow)
    import              Language.Falcon.SemanticAnalyser
    import              Language.Falcon.Precompiler
    
    import              Math.Category
    import              Math.FiniteCategory
    import              Math.CartesianClosedCategory
    import              Math.Categories.FinGrph
    import              Math.Categories.CommaCategory
    import              Math.FiniteCategories.One
    import              Math.FiniteCategories.DiscreteTwo
    import              Math.Categories.FunctorCategory
    import              Math.FiniteCategories.CompositionGraph
    import              Math.Categories.ConeCategory
    import              Math.Categories.FinSketch
    import              Math.IO.PrettyPrint

    import              Data.List                   (intercalate)
    import              Data.WeakSet                (Set)
    import qualified    Data.WeakSet            as  Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap                (Map)
    import qualified    Data.WeakMap            as  Map
    import              Data.WeakMap.Safe
    import              Data.Simplifiable
    import              Data.Maybe                  (fromJust)
    
    import              GHC.Generics
    
    pythonIndent :: Int -> String
    pythonIndent 0 = ""
    pythonIndent x = "    "++(pythonIndent $ x-1)

    
    -- | Transforms an object of the target of a sketch morphism into Python code.
    transformObjectToPythonCode :: SketchMorphism String String -> String -> String
    transformObjectToPythonCode sm obj = case role of
        (ImageObject _) -> "\n"
        (ApexObject c) -> "\n"
        (NadirObject cc) -> "\n"
        (PowerObject t) -> "\n"
        where
            UniquelyDeterminedObject role = getObjectDeterminacy sm obj
            
        
    pythonFunctionPrefix :: String
    pythonFunctionPrefix = "falcon_"
            
    -- | Transform a non identity generating morphism of the target of a sketch morphism into a Haskell identifier.
    morphismToPythonIdentifier :: SketchMorphism String String -> CGMorphism String String -> String
    morphismToPythonIdentifier sm morph
        | isComposite (underlyingCategory $ target sm) morph = error "morphismToPythonIdentifier called on a composite morphism : "++ (show morph)
        | otherwise = case role of
            (ImageMorphism x) -> unsafeGetLabel x
            (ConeLeg c i) -> pythonFunctionPrefix ++ (unsafeGetLabel morph)
            (CoconeLeg cc i) -> pythonFunctionPrefix ++ (unsafeGetLabel morph)
            (EvalMap t) -> pythonFunctionPrefix ++ (unsafeGetLabel morph)
            (IdentityMorphism m) -> error $ "identity morphism given to morphismToPythonIdentifier : "++show morph
            (CompositeMorphism decomp) -> pythonFunctionPrefix ++ (unsafeGetLabel morph)
            (BindingCone c) -> pythonFunctionPrefix ++ (unsafeGetLabel morph)
            (BindingCocone cc) -> pythonFunctionPrefix ++ (unsafeGetLabel morph)
            (BindingTripod t powLeg domLeg codomLeg) -> pythonFunctionPrefix ++ (unsafeGetLabel morph)
            (CompositionLawEntry rp1 rp2) -> error "CompositionLawEntry in morphismToPythonIdentifier"
            where
                UniquelyDeterminedMorphism role = getMorphismDeterminacy sm morph
            
    -- | Transforms a generating morphism of the target of a sketch morphism into Python code.
    transformMorphismToPythonCode :: SketchMorphism String String -> CGMorphism String String -> String
    transformMorphismToPythonCode sm morph = case role of
        (ImageMorphism _) -> "\n"
        (ConeLeg c i) -> "def "++ morphismToPythonIdentifier sm morph ++"(x):\n" ++ pythonIndent 1 ++ "return x[\"" ++ morphismToPythonIdentifier sm morph ++ "\"]\n\n"
        (CoconeLeg cc i) -> "def "++ morphismToPythonIdentifier sm morph ++"(x):\n" ++ pythonIndent 1 ++ "return {\"" ++ morphismToPythonIdentifier sm morph ++ "\" : x}\n\n"
        (EvalMap t) -> "def "++ morphismToPythonIdentifier sm morph ++ "(x):\n" ++ pythonIndent 1 ++ "return x[\"" ++ morphismToPythonIdentifier sm (powerObjectLeg t) ++ "\"](x[\""++ morphismToPythonIdentifier sm (internalDomainLeg t) ++"\"])\n\n"
        (IdentityMorphism m) -> "\n"
        (CompositeMorphism decomp) -> "def " ++ morphismToPythonIdentifier sm morph ++ "(x):\n" ++ pythonIndent 1 ++ "return " ++ (concat ((++"(") <$> (morphismToPythonIdentifier sm) <$> decomp)) ++ "x" ++ replicate (length decomp) ')' ++ "\n\n"
        (BindingCone c) -> "def " ++ morphismToPythonIdentifier sm morph ++ "(x):\n" ++ pythonIndent 1 ++ "y = {}\n" ++ pythonIndent 1 ++ intercalate ("\n"++pythonIndent 1) (Set.setToList ["y[\"" ++ morphismToPythonIdentifier sm ((legsCone c) =>$ i) ++ "\"] = " ++ (concat ((++"(") <$> (morphismToPythonIdentifier sm) <$> (decompose (tgt $ underlyingFunctor sm) (((legsCone c) =>$ i) @ morph)))) ++ "x" ++ replicate (length (decompose (tgt $ underlyingFunctor sm) (((legsCone c) =>$ i) @ morph))) ')' | i <- topObjects]) ++ "\n" ++ pythonIndent 1 ++ "return y\n\n"
            where
                topObjects = [j | j <- ob $ indexingCategoryCone c, Set.null [f | f <- (genArToWithoutId (indexingCategoryCone c) j), Set.null (genArWithoutId (indexingCategoryCone c) (target f) (source f))]]
        (BindingCocone cc) -> if Set.null bottomObjects then "def " ++ morphismToPythonIdentifier sm morph ++ "(x):\n" ++ pythonIndent 1 ++ "raise Exception('Absurd function called.')" else "def " ++ morphismToPythonIdentifier sm morph ++ "(x):\n" ++ Set.concat ([pythonIndent 1 ++ "if \"" ++ morphismToPythonIdentifier sm (legsCocone cc =>$ i) ++ "\" in x:\n" ++ pythonIndent 2 ++ "return " ++ (concat ((++"(") <$> (morphismToPythonIdentifier sm) <$> (decompose (tgt $ underlyingFunctor sm) (morph @ (legsCocone cc =>$ i))))) ++ "x[\"" ++ morphismToPythonIdentifier sm (legsCocone cc =>$ i) ++ "\"]" ++ replicate (length (decompose (tgt $ underlyingFunctor sm) (morph @ (legsCocone cc =>$ i)))) ')' ++ "\n"  | i <- bottomObjects]) ++ "\n"
            where
                bottomObjects = [j | j <- ob $ indexingCategoryCocone cc, Set.null [f | f <- (genArToWithoutId (indexingCategoryCocone cc) j), Set.null (genArWithoutId (indexingCategoryCocone cc) (target f) (source f))]]
        BindingTripod t powLeg domLeg codomLeg -> "def "++ morphismToPythonIdentifier sm morph ++ "(a):\n" ++ pythonIndent 1 ++ "return lambda b : " ++ morphismToPythonIdentifier sm codomLeg ++ "({\""++ morphismToPythonIdentifier sm powLeg ++ "\" : a, \"" ++ morphismToPythonIdentifier sm domLeg ++ "\" : b})\n\n"
        (CompositionLawEntry rp1 rp2) -> error "CompositionLawEntry in transformMorphismToPythonCode"
        where
            UniquelyDeterminedMorphism role = getMorphismDeterminacy sm morph