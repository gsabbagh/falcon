module Main where

import              Language.Falcon.Compiler
import              Options.Applicative

main :: IO ()
main = do
    options <- execParser parseOptionsCompilerWithInfo
    maybeError <- compile options
    if null maybeError then return () else do
        let Just err = maybeError
        putStrLn $ show err
