module Main 
(
    main
) 
where
    import Language.Falcon.Parser
    import Text.Parsec
    --import autres tests

    fromRight :: Either a b -> b
    fromRight (Right x) = x
    fromRight _ = error "fromRight on Left"

    testCategoryConstruct :: IO ()
    testCategoryConstruct = do
        let parsing = parse parseFalcon "" "category test{f : A->B;g : B -> C; h: A -> C; h = g.f;}"
        return ()
        -- let result = constructCategorySketch (Category $ fromRight parsing)
        -- putStrLn $ show $ result
        -- catToPdf (fromRight result) "categorie_parsee"
            

    main :: IO ()
    main = do
      parseTest parseImportStatement testImport1
      parseTest parseImportStatement testImport2
      parseTest parseImportStatement testImport3
      parseTest parseCategoryDeclaration testCategoryDeclaration1
      parseTest parseCategoryDeclaration testCategoryDeclaration2
      parseTest parseFunctorDeclaration testFunctor1
      parseTest parseFunctorDeclaration testFunctor2
      parseTest parseConeDeclaration testCone1
      parseTest parseCoconeDeclaration testCocone1
      parseTest parseExponentialDeclaration testExponentialObject1
      parseTest parseSketchMorphismDeclaration testSketchMorphism1
      parseTest parseFalcon testFalcon1
      testCategoryConstruct
